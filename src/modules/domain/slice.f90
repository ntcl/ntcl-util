module slice_module
    use, intrinsic :: iso_fortran_env, only : int64
    use :: domain_module, only : domain
    implicit none
    private

    public :: slice

    type, extends(domain) :: slice
        integer(int64) :: first, last, offset, nsize
    contains
        generic :: get_overlap => &
                get_overlap_slice
        generic :: expand => expand_slice
        procedure :: get_overlap_slice => get_overlap_slice
        procedure :: expand_slice => expand_slice
        procedure :: populate_meta_information => populate_meta_information
    end type slice

    interface slice
        module procedure constructor_int64
        module procedure constructor_int32
    end interface slice

contains
    pure function constructor_int64(first, last) result(this)
        integer(int64), intent(in) :: first, last
        type(slice) :: this

        call this%clear()
        this%domain = domain([first], [last])
        call this%populate_meta_information()
    end function constructor_int64

    pure function constructor_int32(first, last) result(this)
        integer, intent(in) :: first, last
        type(slice) :: this

        this = slice(int(first, int64), int(last, int64))
    end function constructor_int32

    pure subroutine populate_meta_information(this)
        class(slice), intent(inout) :: this

        this%first = this%all_first(1)
        this%last = this%all_last(1)
        this%offset = this%all_offsets(1)
        this%nsize = this%all_sizes(1)
    end subroutine populate_meta_information

    type(slice) function get_overlap_slice(this, other)
        class(slice), intent(in) :: this
        type(slice), intent(in) :: other

        get_overlap_slice = slice(0,0)
        call this%populate_overlap(get_overlap_slice, other)
        call get_overlap_slice%populate_meta_information()
    end function get_overlap_slice

    type(slice) function expand_slice(this, other)
        class(slice), intent(in) :: this
        type(slice), intent(in) :: other

        expand_slice = slice(0,0)
        call this%populate_expand(expand_slice, other)
        call expand_slice%populate_meta_information()
    end function expand_slice
end module slice_module
