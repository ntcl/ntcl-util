module tile_module
    use, intrinsic :: iso_fortran_env, only : int64, int32
    use :: domain_module, only : domain
    implicit none
    private

    public :: tile

    type, extends(domain) :: tile
        integer(int64) :: row_start
        integer(int64) :: row_end
        integer(int64) :: row_offset
        integer(int64) :: row_size
        integer(int64) :: col_start
        integer(int64) :: col_end
        integer(int64) :: col_offset
        integer(int64) :: col_size
    contains
        generic :: get_overlap => &
                get_overlap_tile
        generic :: expand => &
                expand_tile

        procedure :: get_overlap_tile
        procedure :: expand_tile => expand_tile
        procedure :: populate_meta_information
    end type tile

    interface tile
        module procedure constructor_int32
        module procedure constructor_int64
        module procedure constructor_2int64_2int32
        module procedure constructor_2int64_2int32_v2
    end interface tile
contains
    pure function constructor_int64(rstart, rend, cstart, cend) result(this)
        integer(int64), intent(in) :: rstart
        integer(int64), intent(in) :: rend
        integer(int64), intent(in) :: cstart
        integer(int64), intent(in) :: cend
        type(tile) :: this

        call this%clear()
        this%domain = domain([rstart, cstart], [rend, cend])
        call this%populate_meta_information()
    end function constructor_int64

    pure function constructor_int32(rstart, rend, cstart, cend) result(this)
        integer(int32), intent(in) :: rstart
        integer(int32), intent(in) :: rend
        integer(int32), intent(in) :: cstart
        integer(int32), intent(in) :: cend
        type(tile) :: this

        this = constructor_int64(int(rstart, int64), int(rend, int64), int(cstart, int64), int(cend, int64))
    end function constructor_int32

    pure function constructor_2int64_2int32(rstart, rend, cstart, cend) result(this)
        integer(int64), intent(in) :: rstart
        integer(int64), intent(in) :: rend
        integer(int32), intent(in) :: cstart
        integer(int32), intent(in) :: cend
        type(tile) :: this

        this = constructor_int64(int(rstart, int64), int(rend, int64), int(cstart, int64), int(cend, int64))
    end function constructor_2int64_2int32

    pure function constructor_2int64_2int32_v2(rstart, rend, cstart, cend) result(this)
        integer(int32), intent(in) :: rstart
        integer(int64), intent(in) :: rend
        integer(int32), intent(in) :: cstart
        integer(int64), intent(in) :: cend
        type(tile) :: this

        this = constructor_int64(int(rstart, int64), int(rend, int64), int(cstart, int64), int(cend, int64))
    end function constructor_2int64_2int32_v2

    pure subroutine populate_meta_information(this)
        class(tile), intent(inout) :: this

        this%row_start = this%all_first(1)
        this%col_start = this%all_first(2)

        this%row_end = this%all_last(1)
        this%col_end = this%all_last(2)

        this%row_offset = this%all_offsets(1)
        this%col_offset = this%all_offsets(2)

        this%row_size = this%all_sizes(1)
        this%col_size = this%all_sizes(2)
    end subroutine populate_meta_information

    type(tile) function get_overlap_tile(this, other)
        class(tile), intent(in) :: this
        type(tile), intent(in) :: other

        get_overlap_tile = tile(0,0,0,0)
        call this%populate_overlap(get_overlap_tile, other)
        call get_overlap_tile%populate_meta_information()
    end function get_overlap_tile

    type(tile) function expand_tile(this, other)
        class(tile), intent(in) :: this
        type(tile), intent(in) :: other

        expand_tile = tile(0,0,0,0)
        call this%populate_expand(expand_tile, other)
        call expand_tile%populate_meta_information()
    end function expand_tile
end module tile_module
