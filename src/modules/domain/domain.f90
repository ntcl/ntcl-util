module domain_module
    use :: iso_fortran_env, only : int64
    implicit none
    private

    public :: domain

    type :: domain
        integer :: number_of_dimensions
        integer(int64), dimension(:), allocatable :: all_first, all_last, all_offsets, all_sizes
    contains
        generic :: get_overlap => &
                get_overlap_domain
        procedure :: has_overlap => has_overlap
        procedure :: populate_overlap => populate_overlap
        procedure :: get_overlap_domain => get_overlap_domain
        procedure :: calculate_offsets_and_sizes => calculate_offsets_and_sizes
        procedure :: is_equal => is_equal
        generic :: operator(==) => is_equal
        generic :: expand => expand_domain
        procedure :: expand_domain => expand_domain
        procedure :: populate_expand => populate_expand
        procedure :: clear => clear
        procedure :: cleanup => cleanup
    end type domain

    interface domain
        module procedure constructor_int64
        module procedure constructor_int32
        module procedure constructor_from_number_of_dimensions
    end interface domain
contains
    pure function constructor_from_number_of_dimensions(number_of_dimensions) result(this)
        integer, intent(in) :: number_of_dimensions
        type(domain) :: this

        integer :: idx

        this%number_of_dimensions = number_of_dimensions

        this = domain([(0, idx = 1, number_of_dimensions)], &
                [(0, idx = 1, number_of_dimensions)])
    end function constructor_from_number_of_dimensions

    pure function constructor_int64(all_first, all_last) result(this)
        integer(int64), dimension(:), intent(in) :: all_first, all_last
        type(domain) :: this

        call this%clear()
        if (size(all_first) /= size(all_last)) return

        this%number_of_dimensions = size(all_first)

        this%all_first = all_first
        this%all_last = all_last

        call this%calculate_offsets_and_sizes()
    end function constructor_int64

    pure function constructor_int32(all_first, all_last) result(this)
        integer, dimension(:), intent(in) :: all_first, all_last
        type(domain) :: this

        this = domain(int(all_first, int64), int(all_last, int64))
    end function constructor_int32

    pure subroutine calculate_offsets_and_sizes(this)
        class(domain), intent(inout) :: this

        this%all_offsets = this%all_first - 1
        this%all_sizes = this%all_last - this%all_first + 1
        where ( this%all_sizes < 0) this%all_sizes = 0_int64
    end subroutine calculate_offsets_and_sizes

    logical function has_overlap(this, other)
        class(domain), intent(in) :: this, other

        integer :: d

        if ( .not. same_type_as(this, other) .or. &
                this%number_of_dimensions /= other%number_of_dimensions ) then
            error stop "domain::has_overlap:Incompatible domains."
        end if

        has_overlap = .true.
        do d = 1, this%number_of_dimensions
            if ( this%all_first(d) == other%all_first(d) ) then
                if ( this%all_sizes(d) <= 0 .or. other%all_sizes(d) <= 0 ) &
                        has_overlap = .false.
            else
                if ( this%all_first(d) > other%all_first(d) ) then
                    has_overlap = has_overlap .and. &
                                this%all_first(d) <= other%all_last(d)
                else
                    has_overlap = has_overlap .and. &
                            this%all_last(d) >= other%all_first(d)
                end if
            end if

        end do
    end function has_overlap

    type(domain) function get_overlap_domain(this, other)
        class(domain), intent(in) :: this
        type(domain), intent(in) :: other

        if ( .not. same_type_as(this, other) .or. &
                this%number_of_dimensions /= other%number_of_dimensions ) then
            error stop "domain::get_overlap:Incompatible domains."
        end if

        get_overlap_domain = domain(this%number_of_dimensions)
        call this%populate_overlap(get_overlap_domain, other)
    end function get_overlap_domain

    subroutine populate_overlap(this, overlap, other)
        class(domain), intent(in) :: this, other
        class(domain), intent(inout) :: overlap

        if ( &
                (.not. same_type_as(this, other)) .or. &
                (.not. same_type_as(this, overlap)) .or. &
                this%number_of_dimensions /= other%number_of_dimensions .or. &
                this%number_of_dimensions /= overlap%number_of_dimensions ) then
            error stop "domain::populate_overlap:Incompatible domains."
        end if

        if ( .not. this%has_overlap(other) ) return

        overlap%all_first = max(this%all_first, other%all_first)
        overlap%all_last = min(this%all_last, other%all_last)

        call overlap%calculate_offsets_and_sizes()
    end subroutine populate_overlap

    type(domain) function expand_domain(this, other)
        class(domain), intent(in) :: this
        type(domain), intent(in) :: other

        if ( .not. same_type_as(this, other) .or. &
                this%number_of_dimensions /= other%number_of_dimensions ) then
            error stop "domain::expand:Incompatible domains."
        end if

        expand_domain = domain(this%number_of_dimensions)
        call this%populate_expand(expand_domain, other)
    end function expand_domain

    subroutine populate_expand(this, expand, other)
        class(domain), intent(in) :: this, other
        class(domain), intent(inout) :: expand

        if ( &
                (.not. same_type_as(this, other)) .or. &
                (.not. same_type_as(this, expand)) .or. &
                this%number_of_dimensions /= other%number_of_dimensions .or. &
                this%number_of_dimensions /= expand%number_of_dimensions ) then
            error stop "domain::populate_expand:Incompatible domains."
        end if

        expand%all_first = min(this%all_first, other%all_first)
        expand%all_last = max(this%all_last, other%all_last)

        call expand%calculate_offsets_and_sizes()
    end subroutine populate_expand

    pure function is_equal(this, other) result(res)
        class(domain), intent(in) :: this, other
        logical :: res

        res = &
            allocated(this%all_first) .and. allocated(other%all_first) .and. &
            allocated(this%all_last) .and. allocated(other%all_last) .and. &
            allocated(this%all_offsets) .and. allocated(other%all_offsets) .and. &
            allocated(this%all_sizes) .and. allocated(other%all_sizes)
        if (.not. res) return

        if (this%number_of_dimensions /= other%number_of_dimensions ) res = .false.
        if (size(this%all_first) == size(other%all_first)) then
            res = res .and. all(this%all_first == other%all_first)
        else
            res = .false.
        end if

        if (size(this%all_last) == size(other%all_last)) then
            res = res .and. all(this%all_last == other%all_last)
        else
            res = .false.
        end if

        if (size(this%all_offsets) == size(other%all_offsets)) then
            res = res .and. all(this%all_offsets == other%all_offsets)
        else
            res = .false.
        end if

        if (size(this%all_sizes) == size(other%all_sizes)) then
            res = res .and. all(this%all_sizes == other%all_sizes)
        else
            res = .false.
        end if
    end function is_equal

    pure subroutine cleanup(this)
        class(domain), intent(inout) :: this

        if (allocated(this%all_first)) deallocate(this%all_first)
        if (allocated(this%all_last)) deallocate(this%all_last)
        if (allocated(this%all_offsets)) deallocate(this%all_offsets)
        if (allocated(this%all_sizes)) deallocate(this%all_sizes)

        call this%clear()
    end subroutine cleanup
        
    pure subroutine clear(this)
        class(domain), intent(inout) :: this

        this%number_of_dimensions = 0
    end subroutine clear
end module domain_module
