module configuration_api
    use :: application_config_module
    use :: config_file_parser_module
    use :: options_handler_module

    implicit none
    public
end module configuration_api
