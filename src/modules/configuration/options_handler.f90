module options_handler_module
    use, intrinsic :: iso_fortran_env, only : &
            int64

    use :: string_api, only : &
            string

    use :: dictionary_api, only : &
            dictionary, &
            dictionary_converter

    implicit none
    private

    public :: options_handler

    type :: options_handler
        type(dictionary_converter) :: converter
    contains
        generic :: to_int => &
                to_int_from_chars
        generic :: to_int64 => &
                to_int64_from_chars

        procedure :: to_int_from_chars => to_int_from_chars
        procedure :: to_int64_from_chars => to_int64_from_chars
    end type options_handler

    integer, parameter :: default_int = 0
    integer, parameter :: default_int64 = 0
contains
    integer function to_int_from_chars(this, actual_value, &
            key, options, default_options, priorities, default_value)
        class(options_handler), intent(in) :: this
        integer, intent(in), optional :: actual_value
        character(len=*), intent(in), optional :: key
        type(dictionary), intent(in), optional :: options, default_options
        type(string), dimension(:), intent(in), optional :: priorities
        integer, intent(in), optional :: default_value

        if ( present(actual_value) ) then
            to_int_from_chars = actual_value
            return
        end if

        to_int_from_chars = default_int
        if ( present(default_value) ) then
            to_int_from_chars = default_value
        end if


        if ( present(key) ) then
            to_int_from_chars = this%converter%to_int( key, &
                    default_options, priorities, to_int_from_chars)

            to_int_from_chars = this%converter%to_int( key, &
                    options, priorities, to_int_from_chars)
        end if
    end function to_int_from_chars

    integer(int64) function to_int64_from_chars(this, actual_value, &
            key, options, default_options, priorities, default_value)
        class(options_handler), intent(in) :: this
        integer(int64), intent(in), optional :: actual_value
        character(len=*), intent(in), optional :: key
        type(dictionary), intent(in), optional :: options, default_options
        type(string), dimension(:), intent(in), optional :: priorities
        integer(int64), intent(in), optional :: default_value

        if ( present(actual_value) ) then
            to_int64_from_chars = actual_value
            return
        end if

        to_int64_from_chars = default_int64
        if ( present(default_value) ) then
            to_int64_from_chars = default_value
        end if

        if ( present(key) ) then
            to_int64_from_chars = this%converter%to_int64( key, &
                    default_options, priorities, to_int64_from_chars)

            to_int64_from_chars = this%converter%to_int64( key, &
                    options, priorities, to_int64_from_chars)
        end if
    end function to_int64_from_chars
end module options_handler_module
