module string_api
    use :: string_module, only : string
    use :: string_converter_module
    use :: string_array_helper
    use :: priority_helper

    implicit none
    public
end module string_api
