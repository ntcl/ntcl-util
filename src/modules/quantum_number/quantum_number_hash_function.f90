module quantum_number_hash_function_module
    use :: integer_range_module, only : &
            integer_range

    use :: quantum_number_module, only : &
            quantum_number

    implicit none
    private

    public :: quantum_number_hash_function

    type :: quantum_number_hash_function
    contains
        procedure, nopass :: compute => compute
    end type
contains
    pure integer function compute(quantum_numbers, ranges) result(hash)
        type(quantum_number), dimension(:), intent(in) :: quantum_numbers
        type(integer_range), dimension(:), intent(in) :: ranges

        integer, dimension(:), allocatable :: values, normalized_values, sizes
        integer, dimension(:), allocatable :: multipliers
        integer :: idx

        values = pack( &
                [(quantum_numbers(idx)%get_value(), idx=1,size(quantum_numbers))], &
                [(quantum_numbers(idx)%is_set(), idx=1,size(quantum_numbers))])

        if ( size(values) /= size(ranges) ) &
                error stop 'quantum_number_hash_function::compute:Error in size.'

        normalized_values = (values - [(ranges(idx)%first, idx=1,size(ranges))])/ &
                [(ranges(idx)%stride, idx=1,size(ranges))]

        sizes = [(ranges(idx)%number_of_items, idx=1,size(ranges))]
        multipliers = [1, (product(sizes(1:idx)), idx = 1, size(sizes)-1)]

        hash = 1
        do idx = 1, size(sizes)
            hash = hash + normalized_values(idx)*multipliers(idx)
        end do
    end function compute
end module quantum_number_hash_function_module
