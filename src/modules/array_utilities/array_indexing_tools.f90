module array_indexing_tools
    use, intrinsic :: iso_fortran_env, only : int64

    use :: domain_module, only : domain

    use :: array_view_module, only : &
            array_view

    use :: embedded_array_view_module, only : &
            embedded_array_view

    use :: distributed_array_view_module, only : &
            distributed_array_view

    use :: distributed_embedded_array_view_module, only : &
            distributed_embedded_array_view

    implicit none
    private

    public :: calculate_tensor_indices
    public :: calculate_tensor_indices_on_domain
    public :: get_tensor_index_strides
    public :: get_valid_index_map
    public :: get_source_indices_from_tensor_indices
    public :: get_destination_indices_from_tensor_indices
    public :: calculate_local_array_indices_from_array_view
    public :: calculate_global_array_indices_from_array_view
    public :: calculate_vector_indices_from_local_array_indices
    public :: calculate_vector_indices_from_global_array_indices
    public :: calculate_vector_indices_from_array_view
    public :: calculate_vector_indices_from_embedded_view
    public :: calculate_matrix_indices_from_tensor_view
    public :: combine_local_tensor_indices_to_matrix_indices
    public :: combine_global_tensor_indices_to_matrix_indices
    public :: calculate_valid_vector_indices_in_global_view
    public :: calculate_valid_vector_indices_in_stored_view
    public :: calculate_valid_array_indices_in_global_view
    public :: calculate_valid_array_indices_in_stored_view
    public :: calculate_valid_index_map_from_array_view

    public :: calculate_transformation_indices
    public :: calculate_transformation_indices_from_views
    public :: calculate_transformation_indices_from_embedded_views
    public :: calculate_transformation_indices_of_embedded_view_from_view
    public :: calculate_transformation_indices_of_view_from_embedded_view
contains
    subroutine calculate_tensor_indices(indices, offsets, sizes)
        integer(int64), dimension(:,:), allocatable, intent(inout) :: indices
        integer(int64), dimension(:), intent(in) :: offsets, sizes

        integer :: i, ndim
        integer(int64) :: offset, di, nr, idx, multiplier, total_size, stride, dim_size, dim_offset

        ndim = size(sizes)
        total_size = product(sizes)

        allocate(indices(ndim, total_size))

        multiplier = 1
        do i = 1, ndim
            dim_size = sizes(i)
            dim_offset = offsets(i)
            stride = product(sizes(1:i))
            do offset = 0, total_size-1, stride
                do di = 1, dim_size
                    do nr = 1, multiplier
                        idx = offset + (di-1)*multiplier + nr
                        indices(i, idx) = dim_offset + di
                    end do
                end do
            end do
            multiplier = multiplier * sizes(i)
        end do
    end subroutine calculate_tensor_indices

    subroutine calculate_tensor_indices_on_domain(indices, adomain)
        integer(int64), dimension(:,:), allocatable, intent(inout) :: indices
        class(domain), intent(in) :: adomain

        integer :: d
        integer(int64) :: offset, d_idx, nr, idx, multiplier, total_size, stride, dim_size, dim_offset

        total_size = product(adomain%all_sizes)

        allocate(indices(adomain%number_of_dimensions, total_size))

        multiplier = 1
        do d = 1, adomain%number_of_dimensions
            dim_size = adomain%all_sizes(d)
            dim_offset = adomain%all_offsets(d)
            stride = product(adomain%all_sizes(1:d))
            do offset = 0, total_size-1, stride
                do d_idx = 1, dim_size
                    do nr = 1, multiplier
                        idx = offset + (d_idx-1)*multiplier + nr
                        indices(d, idx) = dim_offset + d_idx
                    end do
                end do
            end do
            multiplier = multiplier * adomain%all_sizes(d)
        end do
    end subroutine calculate_tensor_indices_on_domain

    function get_tensor_index_strides(sizes) result(strides)
        integer(int64), dimension(:), intent(in) :: sizes
        integer(int64), dimension(:), allocatable :: strides

        integer :: idx

        allocate(strides(size(sizes)))
        strides(1) = 1
        do idx = 2, size(sizes)
            strides(idx) = strides(idx-1)*sizes(idx-1)
        end do
    end function get_tensor_index_strides

    function get_valid_index_map(tensor_indices, offsets, sizes) result(valid)
        integer(int64), dimension(:,:), intent(in) :: tensor_indices
        integer(int64), dimension(:), intent(in) :: offsets, sizes
        logical, dimension(:), allocatable :: valid

        integer :: idx, d

        allocate(valid(size(tensor_indices, 2)))

        do idx = 1, size(tensor_indices, 2)
            valid(idx) = .true.
            do d = 1, size(sizes)
                valid(idx) = valid(idx) .and. &
                        tensor_indices(d, idx) > offsets(d) .and. &
                        tensor_indices(d, idx) <= offsets(d) + sizes(d)
            end do
        end do
    end function get_valid_index_map

    function get_source_indices_from_tensor_indices(tensor_indices, offsets, sizes) result(indices)
        integer(int64), dimension(:,:), intent(in) :: tensor_indices
        integer(int64), dimension(:), intent(in) :: offsets, sizes
        integer(int64), dimension(:), allocatable :: indices

        integer(int64), dimension(:), allocatable :: strides
        integer :: idx, d

        strides = get_tensor_index_strides(sizes)

        allocate(indices(size(tensor_indices, 2)))
        do idx = 1, size(tensor_indices, 2)
            indices(idx) = 1
            do d = 1, size(sizes)
                indices(idx) = indices(idx) + &
                    (tensor_indices(d, idx) - offsets(d) - 1)*strides(d)
            end do
        end do

        indices = pack(indices, &
                get_valid_index_map(tensor_indices, offsets, sizes))
    end function get_source_indices_from_tensor_indices

    function get_destination_indices_from_tensor_indices(tensor_indices, offsets, sizes) result(indices)
        integer(int64), dimension(:,:), intent(in) :: tensor_indices
        integer(int64), dimension(:), intent(in) :: offsets, sizes
        integer(int64), dimension(:), allocatable :: indices

        logical, dimension(:), allocatable :: valid
        integer(int64) :: idx

        valid = get_valid_index_map(tensor_indices, offsets, sizes)
        indices = pack([(idx, idx = int(1, int64), size(valid, kind=int64))], valid)
    end function get_destination_indices_from_tensor_indices

    subroutine calculate_transformation_indices( &
            dst_indices, src_indices, dst_offsets, dst_sizes, src_offsets, src_sizes)
        integer(int64), dimension(:), allocatable, intent(inout) :: dst_indices, src_indices
        integer(int64), dimension(:), intent(in) :: dst_offsets, dst_sizes, src_offsets, src_sizes

        integer(int64), dimension(:,:), allocatable :: src_tensor_idx

        call calculate_tensor_indices(src_tensor_idx, src_offsets, src_sizes)

        dst_indices = get_destination_indices_from_tensor_indices( &
                src_tensor_idx, dst_offsets, dst_sizes)
        src_indices = get_source_indices_from_tensor_indices( &
                src_tensor_idx, dst_offsets, dst_sizes)
    end subroutine calculate_transformation_indices

    subroutine calculate_local_array_indices_from_array_view(indices, view)
        integer(int64), dimension(:,:), allocatable, intent(inout) :: indices
        type(array_view), intent(in) :: view

        integer :: i, ndim
        integer(int64) :: offset, di, nr, idx, multiplier, total_size, stride, dim_size

        ndim = size(view%local_sizes)
        total_size = product(view%local_sizes)

        if ( allocated(indices) ) deallocate(indices)
        allocate(indices(ndim, total_size))

        multiplier = 1
        do i = 1, ndim
            dim_size = view%local_sizes(i)
            stride = product(view%local_sizes(1:i))
            do offset = 0, total_size-1, stride
                do di = 1, dim_size
                    do nr = 1, multiplier
                        idx = offset + (di-1)*multiplier + nr
                        indices(i, idx) = di
                    end do
                end do
            end do
            multiplier = multiplier * view%local_sizes(i)
        end do
    end subroutine calculate_local_array_indices_from_array_view

    subroutine calculate_global_array_indices_from_array_view(indices, view)
        integer(int64), dimension(:,:), allocatable, intent(inout) :: indices
        type(array_view), intent(in) :: view

        integer :: i, ndim
        integer(int64) :: offset, di, nr, idx, multiplier, total_size, stride, dim_size, dim_offset

        ndim = size(view%local_sizes)
        total_size = product(view%local_sizes)

        if ( allocated(indices) ) deallocate(indices)
        allocate(indices(ndim, total_size))

        multiplier = 1
        do i = 1, ndim
            dim_size = view%local_sizes(i)
            dim_offset = view%offsets(i)
            stride = product(view%local_sizes(1:i))
            do offset = 0, total_size-1, stride
                do di = 1, dim_size
                    do nr = 1, multiplier
                        idx = offset + (di-1)*multiplier + nr
                        indices(i, idx) = dim_offset + di
                    end do
                end do
            end do
            multiplier = multiplier * view%local_sizes(i)
        end do
    end subroutine calculate_global_array_indices_from_array_view

    subroutine calculate_vector_indices_from_global_array_indices(vector, indices, view)
        integer(int64), dimension(:), allocatable, intent(inout) :: vector
        integer(int64), dimension(:,:), intent(in) :: indices
        type(array_view), intent(in) :: view

        integer(int64), dimension(:), allocatable :: multiplier
        integer :: ndim, d
        integer(int64) :: vectorsize, idx

        ndim = size(view%global_sizes)
        vectorsize = size(indices, 2, kind=int64)
        if ( allocated(vector) ) then
            if ( size(vector) /= vectorsize) deallocate(vector)
        end if

        if ( .not. allocated(vector) ) allocate(vector(vectorsize))

        multiplier = [int(1, int64), (product(view%global_sizes(1:d)), d = 1, ndim-1)]

        do idx = 1, vectorsize
            vector(idx) = 1
            do d = 1, ndim
                vector(idx) = vector(idx) + (indices(d, idx)-1)*multiplier(d)
            end do
        end do
    end subroutine calculate_vector_indices_from_global_array_indices

    subroutine calculate_vector_indices_from_local_array_indices(vector, indices, view)
        integer(int64), dimension(:), allocatable, intent(inout) :: vector
        integer(int64), dimension(:,:), intent(in) :: indices
        type(array_view), intent(in) :: view

        integer(int64), dimension(:), allocatable :: multiplier
        integer :: ndim, d
        integer(int64) :: vectorsize, idx

        ndim = size(view%global_sizes)
        vectorsize = size(indices, 2, kind=int64)
        if ( allocated(vector) ) then
            if ( size(vector) /= vectorsize) deallocate(vector)
        end if
        if ( .not. allocated(vector) ) allocate(vector(vectorsize))

        multiplier = [int(1, int64), (product(view%global_sizes(1:d)), d = 1, ndim-1)]

        do idx = 1, vectorsize
            vector(idx) = 1
            do d = 1, ndim
                vector(idx) = vector(idx) + (view%offsets(d) + indices(d, idx)-1)*multiplier(d)
            end do
        end do
    end subroutine calculate_vector_indices_from_local_array_indices

    subroutine calculate_vector_indices_from_array_view(indices, view)
        integer(int64), dimension(:), allocatable, intent(inout) :: indices
        type(array_view), intent(in) :: view

        integer(int64), dimension(:,:), allocatable :: array_indices

        call calculate_global_array_indices_from_array_view(array_indices, view)
        call calculate_vector_indices_from_global_array_indices(indices, array_indices, view)
    end subroutine calculate_vector_indices_from_array_view

    subroutine calculate_vector_indices_from_embedded_view(vector, view)
        integer(int64), dimension(:), allocatable, intent(inout) :: vector
        type(embedded_array_view), intent(in) :: view

        integer(int64), dimension(:,:), allocatable :: matrix_indices

        call calculate_matrix_indices_from_tensor_view(matrix_indices, view%array, view%number_of_row_indices)
        call calculate_vector_indices_from_local_array_indices(vector, matrix_indices, view%matrix)
    end subroutine calculate_vector_indices_from_embedded_view

    subroutine calculate_matrix_indices_from_tensor_view(indices, tensor_view, number_of_row_indices)
        integer(int64), dimension(:,:), allocatable, intent(inout) :: indices
        type(array_view), intent(in) :: tensor_view
        integer, intent(in) :: number_of_row_indices

        integer(int64), dimension(:,:), allocatable :: tensor_indices

        call calculate_global_array_indices_from_array_view(tensor_indices, tensor_view)
        call combine_global_tensor_indices_to_matrix_indices(indices, &
                tensor_indices, tensor_view, number_of_row_indices)
    end subroutine calculate_matrix_indices_from_tensor_view

    subroutine combine_global_tensor_indices_to_matrix_indices(indices, tensor_indices, view, number_of_row_indices)
        integer(int64), dimension(:,:), allocatable, intent(inout) :: indices
        integer(int64), dimension(:,:), intent(in) :: tensor_indices
        type(array_view), intent(in) :: view
        integer, intent(in) :: number_of_row_indices

        integer :: ndim, row_offset, d
        integer(int64) :: nsize, idx
        integer(int64), dimension(:), allocatable :: row_multiplier, col_multiplier

        ndim = size(view%local_sizes)
        nsize = product(view%local_sizes)
        row_offset = number_of_row_indices

        row_multiplier = [int(1, int64), (product(view%global_sizes(1:d)), d = 1, row_offset-1)]
        col_multiplier = [int(1, int64), (product(view%global_sizes(d:nsize-1)), d = row_offset+1, ndim-1)]

        if ( allocated(indices) ) deallocate(indices)
        allocate(indices(2, nsize))

        do idx = 1, nsize
            indices(1, idx) = 1
            do d = 1, row_offset
                indices(1, idx) = indices(1, idx) + (tensor_indices(d, idx) -1)*row_multiplier(d)
            end do
            indices(2, idx) = 1
            do d = row_offset + 1, ndim
                indices(2, idx) = indices(2, idx) + &
                        (tensor_indices(d, idx) -1)*col_multiplier(d-row_offset)
            end do
        end do
    end subroutine combine_global_tensor_indices_to_matrix_indices

    subroutine combine_local_tensor_indices_to_matrix_indices(indices, tensor_indices, view, &
                number_of_row_indices)
        integer(int64), dimension(:,:), allocatable, intent(inout) :: indices
        integer(int64), dimension(:,:), intent(in) :: tensor_indices
        type(array_view), intent(in) :: view
        integer, intent(in) :: number_of_row_indices

        integer :: ndim, row_offset, d
        integer(int64) :: nsize, idx
        integer(int64), dimension(:), allocatable :: row_multiplier, col_multiplier

        ndim = size(view%local_sizes)
        nsize = product(view%local_sizes)
        row_offset = number_of_row_indices

        row_multiplier = [int(1, int64), (product(view%global_sizes(1:d)), d = 1, row_offset-1)]
        col_multiplier = [int(1, int64), (product(view%global_sizes(d:nsize-1)), d = row_offset+1, ndim-1)]

        if ( allocated(indices) ) deallocate(indices)
        allocate(indices(2, nsize))

        do idx = 1, nsize
            indices(1, idx) = 1
            do d = 1, row_offset
                indices(1, idx) = indices(1, idx) + (view%offsets(d) + tensor_indices(d, idx) -1)*row_multiplier(d)
            end do
            indices(2, idx) = 1
            do d = row_offset + 1, ndim
                indices(2, idx) = indices(2, idx) + &
                        (view%offsets(d) + tensor_indices(d, idx) -1)*col_multiplier(d-row_offset)
            end do
        end do
    end subroutine combine_local_tensor_indices_to_matrix_indices

    subroutine calculate_valid_vector_indices_in_global_view(indices, global_view, valid_view)
        integer(int64), dimension(:), allocatable, intent(inout) :: indices
        type(array_view), intent(in) :: global_view, valid_view

        integer(int64), dimension(:,:), allocatable :: valid_indices

        call calculate_valid_array_indices_in_global_view(valid_indices, global_view, valid_view)
        call calculate_vector_indices_from_global_array_indices(indices, valid_indices, global_view)
    end subroutine calculate_valid_vector_indices_in_global_view

    subroutine calculate_valid_vector_indices_in_stored_view(indices, global_view, valid_view, stored_view)
        integer(int64), dimension(:), allocatable, intent(inout) :: indices
        type(array_view), intent(in) :: global_view, valid_view, stored_view

        integer(int64), dimension(:,:), allocatable :: valid_indices

        call calculate_valid_array_indices_in_stored_view(valid_indices, &
                global_view, valid_view, stored_view)
        call calculate_vector_indices_from_global_array_indices(indices, valid_indices, stored_view)
    end subroutine calculate_valid_vector_indices_in_stored_view

    subroutine calculate_valid_vector_indices_in_global_embedded_view(indices, global_view, valid_view)
        integer(int64), dimension(:), allocatable, intent(inout) :: indices
        type(embedded_array_view), intent(in) :: global_view, valid_view

        integer(int64), dimension(:,:), allocatable :: valid_indices

        call calculate_valid_array_indices_in_global_view(valid_indices, &
                global_view%array, valid_view%array)

        call calculate_vector_indices_from_embedded_array_indices(indices, valid_indices, global_view)
    end subroutine calculate_valid_vector_indices_in_global_embedded_view

    subroutine calculate_valid_vector_indices_in_stored_embedded_view(indices, global_view, valid_view, stored_view)
        integer(int64), dimension(:), allocatable, intent(inout) :: indices
        type(embedded_array_view), intent(in) :: global_view, valid_view, stored_view

        integer(int64), dimension(:,:), allocatable :: valid_indices

        call calculate_valid_array_indices_in_stored_view(valid_indices, &
                global_view%array, valid_view%array, stored_view%array)

        call calculate_vector_indices_from_embedded_array_indices(indices, valid_indices, stored_view)
    end subroutine calculate_valid_vector_indices_in_stored_embedded_view

    subroutine calculate_vector_indices_from_embedded_array_indices(vector_indices, array_indices, view)
        integer(int64), dimension(:), allocatable, intent(inout) :: vector_indices
        integer(int64), dimension(:,:), intent(in) :: array_indices
        type(embedded_array_view), intent(in) :: view

        integer(int64), dimension(:,:), allocatable :: matrix_indices

        call combine_global_tensor_indices_to_matrix_indices(matrix_indices, array_indices, &
                view%array, view%number_of_row_indices)

        call calculate_vector_indices_from_local_array_indices(vector_indices, matrix_indices, view%matrix)
    end subroutine calculate_vector_indices_from_embedded_array_indices

    subroutine calculate_transformation_indices_from_views(indices, dst_view, src_view)
        integer(int64), dimension(:,:), allocatable, intent(inout) :: indices
        type(distributed_array_view), intent(in) :: dst_view, src_view

        integer(int64), dimension(:), allocatable :: src_indices, dst_indices
        integer(int64) :: idx

        call calculate_valid_vector_indices_in_stored_view(src_indices, &
                src_view%global_view, dst_view%global_view, src_view%stored_view)
        call calculate_valid_vector_indices_in_stored_view(dst_indices, &
                dst_view%global_view, src_view%global_view, dst_view%stored_view)

        allocate(indices(2, size(src_indices, kind=int64)))
        do idx = 1, size(indices, 2, kind=int64)
            indices(1,idx) = dst_indices(idx)
            indices(2,idx) = src_indices(idx)
        end do
    end subroutine calculate_transformation_indices_from_views

    subroutine calculate_transformation_indices_from_embedded_views(indices, dst_view, src_view)
        integer(int64), dimension(:,:), allocatable, intent(inout) :: indices
        type(distributed_embedded_array_view), intent(in) :: dst_view, src_view

        integer(int64), dimension(:), allocatable :: src_indices, dst_indices
        integer(int64) :: idx

        call calculate_valid_vector_indices_in_stored_embedded_view(src_indices, &
                src_view%global_view, dst_view%global_view, src_view%stored_view)
        call calculate_valid_vector_indices_in_stored_embedded_view(dst_indices, &
                dst_view%global_view, src_view%global_view, dst_view%stored_view)

        allocate(indices(2, size(src_indices, kind=int64)))
        do idx = 1, size(indices, 2, kind=int64)
            indices(1,idx) = dst_indices(idx)
            indices(2,idx) = src_indices(idx)
        end do
    end subroutine calculate_transformation_indices_from_embedded_views

    subroutine calculate_transformation_indices_of_embedded_view_from_view(indices, dst_view, src_view)
        integer(int64), dimension(:,:), allocatable, intent(inout) :: indices
        type(distributed_embedded_array_view), intent(in) :: dst_view
        type(distributed_array_view), intent(in) :: src_view

        integer(int64), dimension(:), allocatable :: src_indices, dst_indices
        integer(int64) :: idx

        call calculate_valid_indices_in_stored_ev_from_view(dst_indices, &
                dst_view%global_view, src_view%global_view, dst_view%stored_view)
        call calculate_valid_indices_of_view_from_valid_ev_in_stored_view(src_indices, &
                src_view%global_view, dst_view%global_view, src_view%stored_view)

        allocate(indices(2, size(src_indices, kind=int64)))
        do idx = 1, size(indices, 2, kind=int64)
            indices(1,idx) = dst_indices(idx)
            indices(2,idx) = src_indices(idx)
        end do
    end subroutine calculate_transformation_indices_of_embedded_view_from_view

    subroutine calculate_transformation_indices_of_view_from_embedded_view(indices, dst_view, src_view)
        integer(int64), dimension(:,:), allocatable, intent(inout) :: indices
        type(distributed_array_view), intent(in) :: dst_view
        type(distributed_embedded_array_view), intent(in) :: src_view

        integer(int64), dimension(:), allocatable :: src_indices, dst_indices
        integer(int64) :: idx

        call calculate_valid_indices_of_view_from_valid_ev_in_stored_view(dst_indices, &
                dst_view%global_view, src_view%global_view, dst_view%stored_view)
        call calculate_valid_indices_in_stored_ev_from_view(src_indices, &
                src_view%global_view, dst_view%global_view, src_view%stored_view)

        allocate(indices(2, size(src_indices, kind=int64)))
        do idx = 1, size(indices, 2, kind=int64)
            indices(1,idx) = dst_indices(idx)
            indices(2,idx) = src_indices(idx)
        end do
    end subroutine calculate_transformation_indices_of_view_from_embedded_view

    subroutine calculate_valid_array_indices_in_global_view(indices, global_view, valid_view)
        integer(int64), dimension(:,:), allocatable, intent(inout) :: indices
        type(array_view), intent(in) :: global_view, valid_view

        integer(int64), dimension(:,:), allocatable :: all_indices
        logical, dimension(:), allocatable :: valid

        call calculate_global_array_indices_from_array_view(all_indices, global_view)
        call calculate_valid_index_map_from_array_view(valid, all_indices, valid_view)

        call populate_valid_indices_from_mask(indices, all_indices, valid)
    end subroutine calculate_valid_array_indices_in_global_view

    subroutine calculate_valid_array_indices_in_stored_view(indices, global_view, valid_view, stored_view)
        integer(int64), dimension(:,:), allocatable, intent(inout) :: indices
        type(array_view), intent(in) :: global_view, valid_view, stored_view

        integer(int64), dimension(:,:), allocatable :: all_indices
        logical, dimension(:), allocatable :: valid

        call calculate_global_array_indices_from_array_view(all_indices, global_view)
        call calculate_valid_index_map_from_array_view(valid, all_indices, valid_view)
        call calculate_global_array_indices_from_array_view(all_indices, stored_view)

        call populate_valid_indices_from_mask(indices, all_indices, valid)
    end subroutine calculate_valid_array_indices_in_stored_view

    subroutine calculate_valid_indices_in_stored_ev_from_view(indices, &
            global_view, valid_view, stored_view)
        integer(int64), dimension(:), allocatable, intent(inout) :: indices
        type(embedded_array_view), intent(in) :: global_view
        type(array_view), intent(in) :: valid_view
        type(embedded_array_view), intent(in) :: stored_view

        integer(int64), dimension(:,:), allocatable :: valid_indices

        call calculate_valid_matrix_indices_in_stored_embedded_view(valid_indices, &
                global_view, valid_view, stored_view)
        call calculate_vector_indices_from_global_array_indices(indices, valid_indices, stored_view%matrix)
    end subroutine calculate_valid_indices_in_stored_ev_from_view

    subroutine calculate_valid_indices_of_view_from_valid_ev_in_stored_view(indices, &
            global_view, valid_view, stored_view)
        integer(int64), dimension(:), allocatable, intent(inout) :: indices
        type(array_view), intent(in) :: global_view
        type(embedded_array_view), intent(in) :: valid_view
        type(array_view), intent(in) :: stored_view

        integer(int64), dimension(:,:), allocatable :: valid_indices

        call calculate_valid_matrix_indices_in_stored_view_from_valid_ev(valid_indices, &
                global_view, valid_view, stored_view)
        call calculate_vector_indices_from_global_array_indices(indices, valid_indices, stored_view)
    end subroutine calculate_valid_indices_of_view_from_valid_ev_in_stored_view

    subroutine calculate_valid_matrix_indices_in_stored_embedded_view(indices, global_view, valid_view, stored_view)
        integer(int64), dimension(:,:), allocatable, intent(inout) :: indices
        type(embedded_array_view), intent(in) :: global_view
        type(array_view), intent(in) :: valid_view
        type(embedded_array_view), intent(in) :: stored_view

        integer(int64), dimension(:,:), allocatable :: all_indices
        logical, dimension(:), allocatable :: valid

        call calculate_global_matrix_indices_from_embedded_view(all_indices, global_view)
        call calculate_valid_index_map_from_array_view(valid, all_indices, valid_view)
        call calculate_global_matrix_indices_from_embedded_view(all_indices, stored_view)

        call populate_valid_indices_from_mask(indices, all_indices, valid)
    end subroutine calculate_valid_matrix_indices_in_stored_embedded_view

    subroutine calculate_global_matrix_indices_from_embedded_view(indices, view)
        integer(int64), dimension(:,:), allocatable, intent(inout) :: indices
        type(embedded_array_view), intent(in) :: view

        integer(int64), dimension(:,:), allocatable :: matrix_indices

        call calculate_matrix_indices_from_tensor_view(matrix_indices, view%array, view%number_of_row_indices)
        call calculate_global_array_indices_from_local(indices, matrix_indices, view%matrix)
    end subroutine calculate_global_matrix_indices_from_embedded_view

    subroutine calculate_valid_matrix_indices_in_stored_view_from_valid_ev(indices, global_view, valid_view, stored_view)
        integer(int64), dimension(:,:), allocatable, intent(inout) :: indices
        type(array_view), intent(in) :: global_view
        type(embedded_array_view), intent(in) :: valid_view
        type(array_view), intent(in) :: stored_view

        integer(int64), dimension(:,:), allocatable :: all_indices, matrix_indices
        logical, dimension(:), allocatable :: valid_matrix, valid_tensor

        call calculate_global_array_indices_from_array_view(all_indices, global_view)
        call calculate_valid_index_map_from_array_view(valid_matrix, all_indices, valid_view%matrix)
        call populate_valid_indices_from_mask(matrix_indices, all_indices, valid_matrix)

        call convert_global_matrix_indices_to_embedded_array_indices(all_indices, matrix_indices, valid_view)
        call calculate_valid_index_map_from_array_view(valid_tensor, all_indices, valid_view%array)

        call calculate_global_array_indices_from_array_view(all_indices, stored_view)
        call populate_valid_indices_from_mask(matrix_indices, all_indices, valid_matrix)
        call populate_valid_indices_from_mask(indices, matrix_indices, valid_tensor)
    end subroutine calculate_valid_matrix_indices_in_stored_view_from_valid_ev

    subroutine populate_valid_indices_from_mask(valid_indices, all_indices, mask)
        integer(int64), dimension(:,:), allocatable, intent(inout) :: valid_indices
        integer(int64), dimension(:,:), intent(in) :: all_indices
        logical, dimension(:), intent(in) :: mask

        integer(int64) :: idx, counter, number_of_valid_indices, number_of_indices
        integer :: d, nrank

        nrank = size(all_indices, 1)
        number_of_indices = size(all_indices, 2, kind=int64)
        number_of_valid_indices = count(mask, kind=int64)

        if ( allocated(valid_indices) ) deallocate(valid_indices)
        allocate(valid_indices(nrank, number_of_valid_indices))

        counter = 0
        do idx = 1, number_of_indices
            if ( .not. mask(idx) ) cycle
            counter = counter + 1
            do d = 1, nrank
                valid_indices(d, counter) = all_indices(d, idx)
            end do
        end do
    end subroutine populate_valid_indices_from_mask

    subroutine calculate_valid_index_map_from_array_view(valid, indices, view)
        logical, dimension(:), allocatable, intent(inout) :: valid
        integer(int64), dimension(:,:), intent(in) :: indices
        type(array_view), intent(in) :: view

        integer :: d, nrank
        integer(int64) :: idx, number_of_indices

        if ( allocated(valid) ) deallocate(valid)

        nrank = size(view%global_sizes)
        number_of_indices = size(indices, 2, kind=int64)

        allocate(valid(number_of_indices))

        do idx = 1, number_of_indices
            valid(idx) = .true.
            do d = 1, nrank
                valid(idx) = valid(idx) .and. &
                        indices(d, idx) > view%offsets(d) .and. &
                        indices(d, idx) <= view%offsets(d) + view%local_sizes(d)
            end do
        end do
    end subroutine calculate_valid_index_map_from_array_view

    subroutine convert_global_matrix_indices_to_embedded_array_indices(indices, matrix_indices, view)
        integer(int64), dimension(:,:), allocatable, intent(inout) :: indices
        integer(int64), dimension(:,:), allocatable, intent(in) :: matrix_indices
        type(embedded_array_view), intent(in) :: view

        integer :: nrank, d, nr
        integer(int64) :: idx, number_of_indices, gidx
        integer(int64), dimension(:), allocatable :: row_stride, col_stride

        nr = view%number_of_row_indices
        nrank = size(view%array%global_sizes)
        number_of_indices = size(matrix_indices, 2, kind=int64)

        row_stride = [int(1, int64), (product(view%array%global_sizes(1:d)), d = 1, nr-1)]
        col_stride = [int(1, int64), (product(view%array%global_sizes(d:nrank-1)), d = nr+1, nrank-1)]

        if ( allocated(indices) ) deallocate(indices)
        allocate(indices(nrank, number_of_indices))

        do idx = 1, number_of_indices
            gidx = matrix_indices(1, idx) - view%matrix%offsets(1)
            do d = 1, nr
                indices(d, idx) = mod((gidx-1)/row_stride(d), view%array%global_sizes(d)) + 1
            end do

            gidx = matrix_indices(2, idx) - view%matrix%offsets(2)
            do d = nr + 1, nrank
                indices(d, idx) = mod((gidx-1)/col_stride(d-nr), view%array%global_sizes(d)) + 1
            end do
        end do
    end subroutine convert_global_matrix_indices_to_embedded_array_indices

    subroutine calculate_global_array_indices_from_local(global_indices, local_indices, view)
        integer(int64), dimension(:,:), allocatable, intent(inout) :: global_indices
        integer(int64), dimension(:,:), intent(in) :: local_indices
        type(array_view), intent(in) :: view

        integer :: d, nrank
        integer(int64) :: idx, number_of_indices

        nrank = size(local_indices, 1)
        number_of_indices = size(local_indices, 2, kind=int64)

        if ( allocated(global_indices) ) deallocate(global_indices)
        allocate(global_indices(nrank, number_of_indices))

        do idx = 1, number_of_indices
            do d = 1, nrank
                global_indices(d, idx) = view%offsets(d) + local_indices(d, idx)
            end do
        end do
    end subroutine calculate_global_array_indices_from_local
end module array_indexing_tools
