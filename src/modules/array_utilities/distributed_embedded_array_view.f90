module distributed_embedded_array_view_module
    use :: embedded_array_view_module, only : &
            embedded_array_view

    implicit none
    private

    public :: distributed_embedded_array_view

    type :: distributed_embedded_array_view
        type(embedded_array_view) :: global_view
        type(embedded_array_view) :: stored_view
    end type distributed_embedded_array_view

    interface distributed_embedded_array_view
        module procedure constructor
        module procedure constructor_empty
    end interface distributed_embedded_array_view
contains
    function constructor(global_view, stored_view) result(this)
        type(embedded_array_view), intent(in) :: global_view, stored_view
        type(distributed_embedded_array_view) :: this

        this%global_view = global_view
        this%stored_view = stored_view
    end function constructor

    function constructor_empty() result(this)
        type(distributed_embedded_array_view) :: this

        this%global_view = embedded_array_view()
        this%stored_view = embedded_array_view()
    end function constructor_empty
end module distributed_embedded_array_view_module
