module sparse_index_map_translation_module
    use, intrinsic :: iso_fortran_env, only : &
            int64

    use :: sparse_index_map_module, only : &
            sparse_index_map

    implicit none
    private

    public :: sparse_index_map_translation

    type :: sparse_index_map_translation
    contains
        procedure :: execute => execute
        procedure, private :: execute_without_reverse_maps => execute_without_reverse_maps
        procedure, private :: execute_with_reverse_maps => execute_with_reverse_maps
    end type sparse_index_map_translation
contains
    subroutine execute(this, map, dst, src)
        class(sparse_index_map_translation), intent(in) :: this
        integer(int64), dimension(:,:), allocatable, intent(inout) :: map
        type(sparse_index_map), intent(in) :: dst, src

        if ( src%get_particle_rank() /= dst%get_particle_rank() ) &
                error stop 'sparse_index_map_translation::execute:Incompatible maps.'

        if ( src%has_reverse_maps() ) then
            call this%execute_with_reverse_maps(map, dst, src%reverse_maps)
        else
            call this%execute_without_reverse_maps(map, dst, src)
        end if
    end subroutine execute

    subroutine execute_without_reverse_maps(this, map, dst, src)
        class(sparse_index_map_translation), intent(in) :: this
        integer(int64), dimension(:,:), allocatable, intent(inout) :: map
        type(sparse_index_map), intent(in) :: dst, src

        integer(int64), dimension(:), allocatable :: reverse_maps

        call src%allocate_and_setup_reverse_lookup(reverse_maps)
        call this%execute_with_reverse_maps(map, dst, reverse_maps)
    end subroutine execute_without_reverse_maps

    subroutine execute_with_reverse_maps(this, map, dst, reverse_maps)
        class(sparse_index_map_translation), intent(in) :: this
        integer(int64), dimension(:,:), allocatable, intent(inout) :: map
        type(sparse_index_map), intent(in) :: dst
        integer(int64), dimension(:), intent(in) :: reverse_maps

        map = dst%get_translation_map(reverse_maps)
    end subroutine execute_with_reverse_maps

end module sparse_index_map_translation_module
