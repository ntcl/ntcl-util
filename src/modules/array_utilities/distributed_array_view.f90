module distributed_array_view_module
    use :: array_view_module, only : &
            array_view

    implicit none
    private

    public :: distributed_array_view

    type :: distributed_array_view
        type(array_view) :: global_view
        type(array_view) :: stored_view
    end type distributed_array_view

    interface distributed_array_view
        module procedure constructor
        module procedure constructor_empty
    end interface distributed_array_view
contains
    function constructor(global_view, stored_view) result(this)
        type(array_view), intent(in) :: global_view, stored_view
        type(distributed_array_view) :: this

        this%global_view = global_view
        this%stored_view = stored_view
    end function constructor

    function constructor_empty() result(this)
        type(distributed_array_view) :: this

        this%global_view = array_view()
        this%stored_view = array_view()
    end function constructor_empty
end module distributed_array_view_module
