module fourbody_reverse_sparse_index_map_module
    use, intrinsic :: iso_fortran_env, only : &
            int64
        
    use :: sparse_index_map_module, only : &
            sparse_index_map

    implicit none
    private

    public :: fourbody_reverse_sparse_index_map

    type :: fourbody_reverse_sparse_index_map
        integer(int64), dimension(:), allocatable :: reverse_vector
        integer(int64), dimension(:,:,:,:), pointer, contiguous :: reverse_map
    contains
        procedure :: setup_reverse_map => setup_reverse_map
        procedure :: cleanup => cleanup
        procedure :: clear => clear

        procedure, private :: is_valid => is_valid
    end type fourbody_reverse_sparse_index_map

    interface fourbody_reverse_sparse_index_map
        module procedure constructor
    end interface fourbody_reverse_sparse_index_map

contains
    function constructor(map) result(this)
        type(sparse_index_map), intent(in) :: map
        type(fourbody_reverse_sparse_index_map) :: this

        call this%clear()

        if ( .not. this%is_valid(map) ) &
                error stop 'sparse_index_map::constructor:Not a valid fourbody index map.'

        call this%setup_reverse_map(map)
    end function constructor

    subroutine setup_reverse_map(this, map)
        class(fourbody_reverse_sparse_index_map), intent(inout), target :: this
        type(sparse_index_map), intent(in) :: map

        call map%allocate_and_setup_reverse_lookup(this%reverse_vector)

        this%reverse_map( &
                map%sparse_domain%all_first(1):map%sparse_domain%all_last(1), &
                map%sparse_domain%all_first(2):map%sparse_domain%all_last(2), &
                map%sparse_domain%all_first(3):map%sparse_domain%all_last(3), &
                map%sparse_domain%all_first(4):map%sparse_domain%all_last(4)) => &
                this%reverse_vector
    end subroutine setup_reverse_map

    logical function is_valid(this, map)
        class(fourbody_reverse_sparse_index_map), intent(in) :: this
        type(sparse_index_map), intent(in) :: map

        is_valid = map%get_particle_rank() == 4
    end function is_valid

    subroutine cleanup(this)
        class(fourbody_reverse_sparse_index_map), intent(inout) :: this

        if ( allocated(this%reverse_vector) ) deallocate(this%reverse_vector)
        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(fourbody_reverse_sparse_index_map), intent(inout) :: this

        this%reverse_map => null()
    end subroutine clear
end module fourbody_reverse_sparse_index_map_module
