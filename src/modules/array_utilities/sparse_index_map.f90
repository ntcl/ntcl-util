module sparse_index_map_module
    use, intrinsic :: iso_fortran_env, only : &
            int64

    use :: domain_module, only : domain
    use :: slice_module, only : slice

    implicit none
    private

    public :: sparse_index_map

    type :: sparse_index_map
        integer, dimension(:,:), allocatable :: maps
        integer(int64), dimension(:), allocatable :: reverse_maps
        type(slice) :: global_slice
        type(domain) :: sparse_domain
    contains
        procedure :: has_maps => has_maps
        procedure :: get_number_of_maps => get_number_of_maps
        procedure :: get_maps => get_maps
        procedure :: get_pointer_to_maps => get_pointer_to_maps
        procedure :: get_pointer_to_reverse_maps => get_pointer_to_reverse_maps
        procedure :: get_particle_rank => get_particle_rank
        procedure :: cleanup => cleanup
        procedure :: is_equal => is_equal
        procedure :: is_not_equal => is_not_equal
        procedure :: allocate_and_setup_reverse_lookup => allocate_and_setup_reverse_lookup
        procedure :: deallocate_reverse_lookup => deallocate_reverse_lookup
        procedure :: find_reverse_index => find_reverse_index
        procedure :: has_reverse_maps => has_reverse_maps
        procedure :: get_translation_map => get_translation_map
        generic :: operator(.eq.) => is_equal
        generic :: operator(.ne.) => is_not_equal
        procedure :: update_maps => update_maps
        procedure, private :: get_slice => get_slice
        procedure, private :: set_global_slice => set_global_slice
        procedure, private :: set_sparse_domain => set_sparse_domain
        procedure, private :: get_bare_translation_map => get_bare_translation_map
        procedure, private :: clear => clear
    end type sparse_index_map

    interface sparse_index_map
        module procedure constructor
        module procedure constructor_shape
    end interface sparse_index_map

contains
    function constructor_shape(map_size, number_of_maps) result(this)
        integer(int64), intent(in) :: map_size
        integer, intent(in) :: number_of_maps
        type(sparse_index_map) :: this

        call this%clear()
        allocate(this%maps(map_size, number_of_maps))
    end function constructor_shape

    function constructor(maps, global_slice, sparse_domain, calculate_reverse_maps) result(this)
        integer, dimension(:,:), intent(in) :: maps
        type(slice), intent(in), optional :: global_slice
        type(domain), intent(in), optional :: sparse_domain
        logical, intent(in), optional :: calculate_reverse_maps
        type(sparse_index_map) :: this

        call this%clear()
        this%maps = maps

        call this%update_maps(global_slice, sparse_domain, calculate_reverse_maps)
    end function constructor

    subroutine update_maps(this, global_slice, sparse_domain, calculate_reverse_maps)
        class(sparse_index_map), intent(inout) :: this
        type(slice), intent(in), optional :: global_slice
        type(domain), intent(in), optional :: sparse_domain
        logical, intent(in), optional :: calculate_reverse_maps

        call this%set_global_slice(global_slice)
        call this%set_sparse_domain(sparse_domain)
        if ( present(calculate_reverse_maps) ) then
            if ( calculate_reverse_maps ) call this%allocate_and_setup_reverse_lookup(this%reverse_maps)
        end if
    end subroutine update_maps

    subroutine set_global_slice(this, aslice)
        class(sparse_index_map), intent(inout) :: this
        type(slice), intent(in), optional :: aslice

        if ( present(aslice) ) then
            this%global_slice = aslice
        else
            this%global_slice = slice(int(1, kind=int64), size(this%maps, 1, kind=int64))
        end if
    end subroutine set_global_slice

    subroutine set_sparse_domain(this, adomain)
        class(sparse_index_map), intent(inout) :: this
        type(domain), intent(in), optional :: adomain

        integer :: idx

        if ( present(adomain) ) then
            this%sparse_domain = adomain
        else
            this%sparse_domain = domain( &
                    [(1, idx = 1, size(this%maps, 2))], &
                    [(maxval(this%maps(:,idx)), idx = 1, size(this%maps, 2))] )
        end if
    end subroutine set_sparse_domain

    pure logical function has_reverse_maps(this)
        class(sparse_index_map), intent(in) :: this

        has_reverse_maps = allocated(this%reverse_maps)
    end function has_reverse_maps

    subroutine allocate_and_setup_reverse_lookup(this, reverse_maps)
        class(sparse_index_map), intent(in) :: this
        integer(int64), dimension(:), allocatable :: reverse_maps

        integer(int64) :: idx, ridx

        if ( .not. allocated(this%maps)) then
            error stop "sparse_index_map::allocate_and_setup_reverse_lookup:Not properly set up."
        end if

        if ( size(this%sparse_domain%all_sizes) == 0 ) then
            allocate( reverse_maps(0:0))
        else
            allocate(reverse_maps(product(int(this%sparse_domain%all_sizes, kind=int64))))
        end if
        reverse_maps = 0

        do idx = 1, this%get_number_of_maps()
            ridx = this%find_reverse_index(this%maps(idx, :) - int(this%sparse_domain%all_offsets))
            reverse_maps(ridx) = idx + this%global_slice%offset
        end do
    end subroutine allocate_and_setup_reverse_lookup

    integer(int64) function find_reverse_index(this, x)
        class(sparse_index_map), intent(in) :: this
        integer, dimension(:), intent(in) :: x

        integer :: idx
        integer(int64) :: offset

        offset = 0
        do idx = this%sparse_domain%number_of_dimensions, 2, -1
            offset = offset + (x(idx) - 1)*product(int(this%sparse_domain%all_sizes(:idx-1), kind=int64))
        end do
        find_reverse_index = offset + x(1)
    end function find_reverse_index

    function get_translation_map(this, reverse_maps) result(map)
        class(sparse_index_map), intent(in) :: this
        integer(int64), dimension(:), intent(in) :: reverse_maps
        integer(int64), dimension(:,:), allocatable :: map

        integer(int64) :: idx, counter, number_of_maps
        integer(int64), dimension(:), allocatable :: new_map

        if ( this%get_particle_rank() == 0 ) then
            map = reshape(int([1,1], int64), shape=[1,2])
            return
        end if

        new_map = this%get_bare_translation_map(reverse_maps)

        number_of_maps = count(new_map>0, kind=int64)
        allocate(map(number_of_maps, 2))
        counter = 0
        do idx = 1, this%get_number_of_maps()
            if ( new_map(idx) < 1 ) cycle
            counter = counter + 1
            map(counter, :) = [idx + this%global_slice%offset, new_map(idx)]
        end do
    end function get_translation_map

    function get_bare_translation_map(this, reverse_maps) result(map)
        class(sparse_index_map), intent(in) :: this
        integer(int64), dimension(:), intent(in) :: reverse_maps
        integer(int64), dimension(:), allocatable :: map

        integer(int64) :: idx

        allocate(map(this%get_number_of_maps()))
        map = 0
        do idx = 1, this%get_number_of_maps()
            map(idx) = reverse_maps(this%find_reverse_index(this%maps(idx, :) - int(this%sparse_domain%all_offsets)))
        end do
    end function get_bare_translation_map

    subroutine deallocate_reverse_lookup(this)
        class(sparse_index_map), intent(inout) :: this

        if (allocated(this%reverse_maps)) deallocate(this%reverse_maps)
    end subroutine deallocate_reverse_lookup

    pure logical function is_equal(this, other)
        class(sparse_index_map), intent(in) :: this, other

        is_equal = this%global_slice == other%global_slice .and. &
                this%sparse_domain == other%sparse_domain .and. &
                (allocated(this%maps) .eqv. allocated(other%maps))

        if (is_equal .and. allocated(this%maps) .and. allocated(other%maps) ) then
            is_equal = is_equal .and. &
                    all(shape(this%maps) == shape(other%maps)) .and. &
                    all(this%maps == other%maps)
        end if
    end function is_equal

    pure logical function is_not_equal(this, other)
        class(sparse_index_map), intent(in) :: this, other

        is_not_equal = .not. this == other
    end function is_not_equal

    pure logical function has_maps(this)
        class(sparse_index_map), intent(in) :: this

        has_maps = this%get_number_of_maps() > 0
    end function has_maps

    pure integer(int64) function get_number_of_maps(this)
        class(sparse_index_map), intent(in) :: this

        get_number_of_maps = size(this%maps, 1, kind=int64)
    end function get_number_of_maps

    pure function get_maps(this, aslice) result(configs)
        class(sparse_index_map), intent(in) :: this
        type(slice), intent(in), optional :: aslice
        integer, dimension(:,:), allocatable :: configs

        type(slice) :: local_slice

        local_slice = slice(int(1, int64), this%get_number_of_maps())
        if (present(aslice)) local_slice = aslice

        if ( local_slice%first < 1 .or. local_slice%last > this%get_number_of_maps() ) &
                error stop 'sparse_index_map::get_maps:Out of bounds.'

        configs = this%get_slice(local_slice)
    end function get_maps

    function get_pointer_to_maps(this) result(configs)
        class(sparse_index_map), intent(in), target :: this
        integer, dimension(:,:), contiguous, pointer :: configs

        configs => this%maps
    end function get_pointer_to_maps

    function get_pointer_to_reverse_maps(this) result(reverse)
        class(sparse_index_map), intent(in), target :: this
        integer(int64), dimension(:), contiguous, pointer :: reverse

        if ( allocated(this%reverse_maps) ) then
            reverse => this%reverse_maps
        else
            error stop "sparse_index_map::get_pointer_to_reverse_maps:Reverse maps not allocated!"
        end if
    end function get_pointer_to_reverse_maps

    integer function get_particle_rank(this)
        class(sparse_index_map), intent(in) :: this

        get_particle_rank = 0
        if (allocated(this%maps)) then
            get_particle_rank = size(this%maps, 2)
            if (size(this%maps) == 1) then
                if (this%maps(1,1) == 0) get_particle_rank = 0
            end if
        end if
    end function get_particle_rank

    pure function get_slice(this, aslice) result(maps)
        class(sparse_index_map), intent(in) :: this
        type(slice), intent(in) :: aslice
        integer, dimension(:,:), allocatable :: maps

        maps = this%maps(aslice%first:aslice%last,:)
    end function get_slice

    subroutine cleanup(this)
        class(sparse_index_map), intent(inout) :: this

        if (allocated(this%maps)) deallocate(this%maps)
        if (allocated(this%reverse_maps)) deallocate(this%reverse_maps)
        call this%global_slice%cleanup()
        call this%sparse_domain%cleanup()
        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(sparse_index_map), intent(inout) :: this

        call this%global_slice%clear()
        call this%sparse_domain%clear()
    end subroutine clear
end module sparse_index_map_module
