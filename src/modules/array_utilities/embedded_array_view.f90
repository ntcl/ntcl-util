module embedded_array_view_module
    use :: array_view_module, only : &
            array_view

    implicit none
    private

    public :: embedded_array_view

    type :: embedded_array_view
        type(array_view) :: matrix, array
        integer :: number_of_row_indices
    end type embedded_array_view

    interface embedded_array_view
        module procedure constructor
        module procedure constructor_empty
    end interface embedded_array_view

contains
    function constructor(array, matrix, number_of_row_indices) result(this)
        type(array_view), intent(in) :: array, matrix
        integer, intent(in) :: number_of_row_indices
        type(embedded_array_view) :: this

        this%matrix = matrix
        this%array = array
        this%number_of_row_indices = number_of_row_indices
    end function constructor

    function constructor_empty()result(this)
        type(embedded_array_view) :: this

        this%matrix = array_view()
        this%array = array_view()
        this%number_of_row_indices = 0
    end function constructor_empty
end module embedded_array_view_module
