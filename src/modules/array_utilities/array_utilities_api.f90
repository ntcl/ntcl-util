module array_utilities_api
    use, intrinsic :: iso_fortran_env, only : int64

    use :: tile_module, only : tile
    use :: array_indexing_tools
    use :: sparse_index_map_module
    use :: sparse_index_map_translation_module
    use :: twobody_reverse_sparse_index_map_module
    use :: threebody_reverse_sparse_index_map_module
    use :: fourbody_reverse_sparse_index_map_module
    use :: array_view_module, only : array_view
    use :: distributed_array_view_module, only : distributed_array_view
    use :: embedded_array_view_module, only : embedded_array_view
    use :: distributed_embedded_array_view_module, only : distributed_embedded_array_view

    implicit none
    private

    public :: array_view
    public :: distributed_array_view
    public :: embedded_array_view
    public :: distributed_embedded_array_view

    public :: calculate_tensor_indices
    public :: calculate_tensor_indices_on_domain
    public :: get_tensor_index_strides
    public :: get_valid_index_map
    public :: get_source_indices_from_tensor_indices
    public :: get_destination_indices_from_tensor_indices
    public :: calculate_local_array_indices_from_array_view
    public :: calculate_global_array_indices_from_array_view
    public :: calculate_vector_indices_from_local_array_indices
    public :: calculate_vector_indices_from_global_array_indices
    public :: calculate_vector_indices_from_array_view
    public :: calculate_vector_indices_from_embedded_view
    public :: calculate_matrix_indices_from_tensor_view
    public :: combine_local_tensor_indices_to_matrix_indices
    public :: combine_global_tensor_indices_to_matrix_indices
    public :: calculate_valid_vector_indices_in_global_view
    public :: calculate_valid_vector_indices_in_stored_view
    public :: calculate_valid_array_indices_in_global_view
    public :: calculate_valid_array_indices_in_stored_view
    public :: calculate_valid_index_map_from_array_view

    public :: calculate_transformation_indices
    public :: calculate_transformation_indices_from_views
    public :: calculate_transformation_indices_from_embedded_views
    public :: calculate_transformation_indices_of_embedded_view_from_view
    public :: calculate_transformation_indices_of_view_from_embedded_view

    public :: sparse_index_map
    public :: sparse_index_map_translation
    public :: twobody_reverse_sparse_index_map
    public :: threebody_reverse_sparse_index_map
    public :: fourbody_reverse_sparse_index_map

    public :: translate_sparse_index_map

    public :: get_tile_from_view
contains
    subroutine translate_sparse_index_map(map, dst, src)
        integer(int64), dimension(:,:), allocatable, intent(inout) :: map
        type(sparse_index_map), intent(in) :: dst, src

        type(sparse_index_map_translation) :: translator

        call translator%execute(map, dst, src)
    end subroutine translate_sparse_index_map

    type(tile) function get_tile_from_view(view) result(atile)
        type(array_view), intent(in) :: view

        if ( size(view%offsets) /= 2 ) &
                error stop 'get_tile_from_view::Error in dimensions.'

        atile = tile( &
                view%offsets(1) + 1, &
                view%offsets(1) + view%local_sizes(1), &
                view%offsets(2) + 1, &
                view%offsets(2) + view%local_sizes(2) )
    end function get_tile_from_view
end module array_utilities_api
