module array_view_module
    use, intrinsic :: iso_fortran_env, only : &
            int64

    implicit none
    private

    public :: array_view

    type :: array_view
        integer(int64), dimension(:), allocatable :: global_sizes, offsets, local_sizes
    end type array_view

    interface array_view
        module procedure constructor
        module procedure constructor_empty
        module procedure constructor_complete
    end interface array_view

contains
    function constructor(global_sizes, offsets, local_sizes) result(this)
        integer(int64), dimension(:), intent(in) :: global_sizes, offsets, local_sizes
        type(array_view) :: this

        this%global_sizes = global_sizes
        this%offsets = offsets
        this%local_sizes = local_sizes
    end function constructor

    function constructor_empty()result(this)
        type(array_view) :: this

    end function constructor_empty

    function constructor_complete(sizes) result(this)
        integer(int64), dimension(:), intent(in) :: sizes
        type(array_view) :: this

        integer :: idx

        this = array_view(sizes, int([(0, idx = 1, size(sizes))], int64), sizes)
    end function constructor_complete
end module array_view_module
