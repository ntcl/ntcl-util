module abc_solver_module
    use, intrinsic :: iso_fortran_env, only : real64

    implicit none
    private

    public :: abc_solver

    type :: abc_solver
        real(real64) :: a, b, c
        logical :: error
    contains
        procedure :: get_largest_solution => get_largest_solution
        procedure :: get_smallest_solution => get_smallest_solution
        procedure :: test_illegal => test_illegal
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type abc_solver

    interface abc_solver
        module procedure constructor
    end interface abc_solver

contains
    function constructor(a, b, c) result(this)
        real(real64), intent(in) :: a, b, c
        type(abc_solver) :: this

        call this%clear()
        this%a = a; this%b = b; this%c = c
    end function constructor

    real(real64) function get_largest_solution(this)
        class(abc_solver), intent(inout) :: this

        get_largest_solution = 0.0d0
        call this%test_illegal()
        if ( this%error ) return

        get_largest_solution = (-this%b + sqrt(this%b**2 - 4*this%a*this%c))/(2*this%a)
    end function get_largest_solution

    real(real64) function get_smallest_solution(this)
        class(abc_solver), intent(inout) :: this

        get_smallest_solution = 0.0
        call this%test_illegal()
        if ( this%error ) return

        get_smallest_solution = (-this%b - sqrt(this%b**2 - 4*this%a*this%c))/(2*this%a)
    end function get_smallest_solution

    subroutine test_illegal(this)
        class(abc_solver), intent(inout) :: this

        this%error = .false.
        if (this%b**2 - 4*this%a*this%c < 0.0d0) then
            this%error = .true.
        end if
    end subroutine test_illegal

    subroutine cleanup(this)
        class(abc_solver), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(abc_solver), intent(inout) :: this

        this%error = .false.
    end subroutine clear
end module abc_solver_module
