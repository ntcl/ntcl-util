#include <stdint.h>
#include <stdio.h>

void* increment_pointer(void *ptr, int64_t number_of_bytes)
{
        return (void*)((char*)ptr + number_of_bytes);
}

void* print_pointer(void *ptr)
{
  printf("Address = %p\n", ptr);
}
