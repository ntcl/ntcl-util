module dictionary_helper_module
    use :: string_module, only : string
    use :: dictionary_module, only : dictionary

    implicit none
    private

    public :: dictionary_helper

    type :: dictionary_helper
    contains
        procedure :: update_options_with_value => update_options_with_value
    end type dictionary_helper
contains
    subroutine update_options_with_value(this, options, key, val, priorities, replace)
        class(dictionary_helper), intent(in) :: this
        type(dictionary), intent(inout) :: options
        character(len=*), intent(in) :: key
        character(len=*), intent(in), optional :: val
        type(string), dimension(:), intent(in), optional :: priorities
        logical, intent(in), optional :: replace

        integer :: idx
        logical :: my_replace

        my_replace = .true.
        if ( present(replace) ) my_replace = replace

        if ( present(val) ) then
            if ( my_replace .or. .not. options%has_key(key) ) &
                    call options%set_value(key, val)
        end if

        if ( present(priorities ) .and. present(val)) then
            do idx = 1, size(priorities)
                if ( my_replace .or. .not. options%has_key(priorities(idx)%char_array//key) ) &
                        call options%set_value(priorities(idx)%char_array//key, val)
            end do
        end if
    end subroutine update_options_with_value
end module dictionary_helper_module
