module dictionary_converter_module
    use, intrinsic :: iso_fortran_env, only : &
            int64,&
            real32, &
            real64

    use :: dictionary_module, only : dictionary
    use :: string_module, only : string
    use :: string_converter_module, only : string_converter

    implicit none
    private

    public :: dictionary_converter

    type :: dictionary_converter
        type(string_converter) :: converter
    contains
        generic :: to_string => &
                to_string_from_string, &
                to_string_from_chars
        generic :: to_string_array => &
                to_string_array_from_string, &
                to_string_array_from_chars
        generic :: to_logical => &
                to_logical_from_chars, &
                to_logical_from_string
        generic :: to_int => &
                to_int_from_chars, &
                to_int_from_string
        generic :: to_int64 => &
                to_int64_from_chars, &
                to_int64_from_string
        generic :: to_real32 => &
                to_real32_from_chars, &
                to_real32_from_string
        generic :: to_real64 => &
                to_real64_from_chars, &
                to_real64_from_string
        generic :: to_string_with_defaults => &
                to_string_with_defaults_from_string, &
                to_string_with_defaults_from_chars
        generic :: to_int_with_defaults => &
                to_int_with_defaults_from_string, &
                to_int_with_defaults_from_chars
        generic :: to_int64_with_defaults => &
                to_int64_with_defaults_from_string, &
                to_int64_with_defaults_from_chars

        procedure :: to_logical_from_chars => to_logical_from_chars
        procedure :: to_logical_from_string => to_logical_from_string
        procedure :: to_int_from_chars => to_int_from_chars
        procedure :: to_int_from_string => to_int_from_string
        procedure :: to_int64_from_chars => to_int64_from_chars
        procedure :: to_int64_from_string => to_int64_from_string
        procedure :: to_string_from_string => to_string_from_string
        procedure :: to_string_from_chars => to_string_from_chars
        procedure :: to_string_array_from_string => to_string_array_from_string
        procedure :: to_string_array_from_chars => to_string_array_from_chars
        procedure :: to_real32_from_chars => to_real32_from_chars
        procedure :: to_real32_from_string => to_real32_from_string
        procedure :: to_real64_from_chars => to_real64_from_chars
        procedure :: to_real64_from_string => to_real64_from_string

        procedure :: to_string_with_defaults_from_string => &
                to_string_with_defaults_from_string
        procedure :: to_string_with_defaults_from_chars => &
                to_string_with_defaults_from_chars
        procedure :: to_int_with_defaults_from_string => &
                to_int_with_defaults_from_string
        procedure :: to_int_with_defaults_from_chars => &
                to_int_with_defaults_from_chars
        procedure :: to_int64_with_defaults_from_string => &
                to_int64_with_defaults_from_string
        procedure :: to_int64_with_defaults_from_chars => &
                to_int64_with_defaults_from_chars
    end type dictionary_converter

    logical, parameter :: default_logical = .false.
    integer(int64), parameter :: default_int64 = 0_int64
    integer, parameter :: default_int = 0
    type(string), dimension(0) :: default_string_array
    real(real32), parameter :: default_real32 = 0.0
    real(real64), parameter :: default_real64 = 0.0_real64
contains
    logical function to_logical_from_chars(this, key, options, priorities, default_value)
        class(dictionary_converter), intent(in) :: this
        character(len=*), intent(in) :: key
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        logical, intent(in), optional :: default_value

        to_logical_from_chars = this%to_logical(string(key), options, priorities, default_value)
    end function to_logical_from_chars

    logical function to_logical_from_string(this, key, options, priorities, default_value)
        class(dictionary_converter), intent(in) :: this
        type(string), intent(in) :: key
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        logical, intent(in), optional :: default_value

        to_logical_from_string = default_logical
        if ( present(default_value) ) to_logical_from_string = default_value

        if ( present(options) ) then
            if ( options%has_key(key, priorities) ) &
                to_logical_from_string = this%converter%to_logical(options%get_value(key, priorities))
        end if
    end function to_logical_from_string

    integer function to_int_from_chars(this, key, options, priorities, default_value)
        class(dictionary_converter), intent(in) :: this
        character(len=*), intent(in) :: key
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        integer, intent(in), optional :: default_value

        to_int_from_chars = this%to_int(string(key), options, priorities, default_value)
    end function to_int_from_chars

    integer function to_int_from_string(this, key, options, priorities, default_value)
        class(dictionary_converter), intent(in) :: this
        type(string), intent(in) :: key
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        integer, intent(in), optional :: default_value

        to_int_from_string = default_int
        if ( present(default_value) ) to_int_from_string = default_value

        if ( present(options) ) then
            if ( options%has_key(key, priorities) ) &
                to_int_from_string = this%converter%toint(options%get_value(key, priorities))
        end if
    end function to_int_from_string

    integer(int64) function to_int64_from_chars(this, key, options, priorities, default_value)
        class(dictionary_converter), intent(in) :: this
        character(len=*), intent(in) :: key
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        integer(int64), intent(in), optional :: default_value

        to_int64_from_chars = this%to_int64(string(key), options, priorities, default_value)
    end function to_int64_from_chars

    integer(int64) function to_int64_from_string(this, key, options, priorities, default_value)
        class(dictionary_converter), intent(in) :: this
        type(string), intent(in) :: key
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        integer(int64), intent(in), optional :: default_value

        to_int64_from_string = default_int64
        if ( present(default_value) ) to_int64_from_string = default_value

        if ( present(options) ) then
            if ( options%has_key(key, priorities) ) &
                to_int64_from_string = this%converter%toint64(options%get_value(key, priorities))
        end if
    end function to_int64_from_string

    type(string) function to_string_from_string(this, key, options, priorities, default_value)
        class(dictionary_converter), intent(in) :: this
        type(string), intent(in) :: key
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        type(string), intent(in), optional :: default_value

        to_string_from_string = ''
        if ( present(default_value) ) to_string_from_string = default_value

        if ( present(options) ) then
            if ( options%has_key(key, priorities) ) &
                to_string_from_string = options%get_value(key, priorities)
        end if
    end function to_string_from_string

    type(string) function to_string_from_chars(this, key, options, priorities, default_value)
        class(dictionary_converter), intent(in) :: this
        character(len=*), intent(in) :: key
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        character(len=*), intent(in), optional :: default_value

        if ( present(default_value) ) then
            to_string_from_chars = this%to_string(string(key), options, priorities, string(default_value))
        else
            to_string_from_chars = this%to_string(string(key), options, priorities)
        end if
    end function to_string_from_chars

    function to_string_array_from_string(this, key, options, priorities, delimiter, default_value) result(array)
        class(dictionary_converter), intent(in) :: this
        type(string), intent(in) :: key
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        character, intent(in), optional :: delimiter
        type(string), dimension(:), intent(in), optional :: default_value
        type(string), dimension(:), allocatable :: array

        if ( present(options) ) then
            if ( options%has_key(key, priorities) ) &
                array = this%converter%to_string_array(options%get_value(key, priorities), delimiter)
        end if

        if (.not. allocated(array) ) then
            if ( present(default_value) ) then
                array = default_value
            else
                array = default_string_array
            end if
        end if
    end function to_string_array_from_string

    function to_string_array_from_chars(this, key, options, priorities, delimiter, default_value) result(array)
        class(dictionary_converter), intent(in) :: this
        character(len=*), intent(in) :: key
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        character, intent(in), optional :: delimiter
        type(string), dimension(:), intent(in), optional :: default_value
        type(string), dimension(:), allocatable :: array

        array = this%to_string_array(string(key), options, priorities, delimiter, default_value)
    end function to_string_array_from_chars

    real(real32) function to_real32_from_chars(this, key, options, priorities, default_value)
        class(dictionary_converter), intent(in) :: this
        character(len=*), intent(in) :: key
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        real(real32), intent(in), optional :: default_value

        to_real32_from_chars = this%to_real32(string(key), options, priorities, default_value)
    end function to_real32_from_chars

    real(real32) function to_real32_from_string(this, key, options, priorities, default_value)
        class(dictionary_converter), intent(in) :: this
        type(string), intent(in) :: key
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        real(real32), intent(in), optional :: default_value

        to_real32_from_string = default_real32
        if ( present(default_value) ) to_real32_from_string = default_value

        if ( present(options) ) then
            if ( options%has_key(key, priorities) ) &
                to_real32_from_string = this%converter%toreal32(options%get_value(key, priorities))
        end if
    end function to_real32_from_string

    real(real64) function to_real64_from_chars(this, key, options, priorities, default_value)
        class(dictionary_converter), intent(in) :: this
        character(len=*), intent(in) :: key
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        real(real64), intent(in), optional :: default_value

        to_real64_from_chars = this%to_real64(string(key), options, priorities, default_value)
    end function to_real64_from_chars

    real(real64) function to_real64_from_string(this, key, options, priorities, default_value)
        class(dictionary_converter), intent(in) :: this
        type(string), intent(in) :: key
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        real(real64), intent(in), optional :: default_value

        to_real64_from_string = default_real64
        if ( present(default_value) ) to_real64_from_string = default_value

        if ( present(options) ) then
            if ( options%has_key(key, priorities) ) &
                to_real64_from_string = this%converter%toreal64(options%get_value(key, priorities))
        end if
    end function to_real64_from_string

    type(string) function to_string_with_defaults_from_string(this, key, default_options, &
            options, priorities, default_value)
        class(dictionary_converter), intent(in) :: this
        type(string), intent(in) :: key
        type(dictionary), intent(in) :: default_options
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        type(string), intent(in), optional :: default_value

        to_string_with_defaults_from_string = ''
        if ( default_options%has_key(key, priorities) ) then
            to_string_with_defaults_from_string = default_options%get_value(key, priorities)
        else if ( present(default_value) ) then
            to_string_with_defaults_from_string = default_value
        end if

        if ( present(options) ) then
            if ( options%has_key(key, priorities) ) &
                to_string_with_defaults_from_string = options%get_value(key, priorities)
        end if
    end function to_string_with_defaults_from_string

    type(string) function to_string_with_defaults_from_chars(this, key, default_options, &
            options, priorities, default_value)
        class(dictionary_converter), intent(in) :: this
        character(len=*), intent(in) :: key
        type(dictionary), intent(in) :: default_options
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        character(len=*), intent(in), optional :: default_value

        if (present(default_value) ) then
            to_string_with_defaults_from_chars = &
                    this%to_string_with_defaults(string(key), default_options, &
                    options, priorities, string(default_value))
        else
            to_string_with_defaults_from_chars = &
                    this%to_string_with_defaults(string(key), default_options, &
                    options, priorities)
        end if
    end function to_string_with_defaults_from_chars

    integer function to_int_with_defaults_from_string(this, key, default_options, &
            options, priorities, default_value)
        class(dictionary_converter), intent(in) :: this
        type(string), intent(in) :: key
        type(dictionary), intent(in) :: default_options
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        integer, intent(in), optional :: default_value

        to_int_with_defaults_from_string = default_int
        if ( default_options%has_key(key, priorities) ) then
            to_int_with_defaults_from_string = this%converter%toint( &
                    default_options%get_value(key, priorities))
        else if ( present(default_value) ) then
            to_int_with_defaults_from_string = default_value
        end if

        if ( present(options) ) then
            if ( options%has_key(key, priorities) ) &
                to_int_with_defaults_from_string = this%converter%toint(options%get_value(key, priorities))
        end if
    end function to_int_with_defaults_from_string

    integer function to_int_with_defaults_from_chars(this, key, default_options, &
            options, priorities, default_value)
        class(dictionary_converter), intent(in) :: this
        character(len=*), intent(in) :: key
        type(dictionary), intent(in) :: default_options
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        integer, intent(in), optional :: default_value

        to_int_with_defaults_from_chars = &
                this%to_int_with_defaults(string(key), default_options, &
                options, priorities, default_value)
    end function to_int_with_defaults_from_chars

    integer(int64) function to_int64_with_defaults_from_string(this, key, default_options, &
            options, priorities, default_value)
        class(dictionary_converter), intent(in) :: this
        type(string), intent(in) :: key
        type(dictionary), intent(in) :: default_options
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        integer(int64), intent(in), optional :: default_value

        to_int64_with_defaults_from_string = default_int
        if ( default_options%has_key(key, priorities) ) then
            to_int64_with_defaults_from_string = this%converter%toint64( &
                    default_options%get_value(key, priorities))
        else if ( present(default_value) ) then
            to_int64_with_defaults_from_string = default_value
        end if

        if ( present(options) ) then
            if ( options%has_key(key, priorities) ) &
                to_int64_with_defaults_from_string = this%converter%toint64( &
                        options%get_value(key, priorities))
        end if
    end function to_int64_with_defaults_from_string

    integer(int64) function to_int64_with_defaults_from_chars(this, key, default_options, &
            options, priorities, default_value)
        class(dictionary_converter), intent(in) :: this
        character(len=*), intent(in) :: key
        type(dictionary), intent(in) :: default_options
        type(dictionary), intent(in), optional :: options
        type(string), dimension(:), intent(in), optional :: priorities
        integer(int64), intent(in), optional :: default_value

        to_int64_with_defaults_from_chars = &
                this%to_int64_with_defaults(string(key), default_options, &
                options, priorities, default_value)
    end function to_int64_with_defaults_from_chars
end module dictionary_converter_module
