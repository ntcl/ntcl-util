module small_ring_counter_module
    implicit none
    private

    public :: small_ring_counter

    type :: small_ring_counter
        integer :: counter_size, current
    contains
        procedure :: get_next => get_next
        procedure :: get_next_and_increase => get_next_and_increase
        procedure :: get_next_and_decrease => get_next_and_decrease
        procedure :: increase => increase
        procedure :: decrease => decrease
        procedure :: reset => reset

        procedure, private :: get_counter_modulo => get_counter_modulo
        procedure, private :: clear => clear
    end type small_ring_counter

    interface small_ring_counter
        module procedure constructor_empty
        module procedure constructor
    end interface small_ring_counter

contains
    function constructor_empty() result(this)
        type(small_ring_counter) :: this

        call this%clear()
    end function constructor_empty

    function constructor(counter_size) result(this)
        integer, intent(in) :: counter_size
        type(small_ring_counter) :: this

        this = small_ring_counter()
        this%counter_size = counter_size
    end function constructor

    integer function get_next(this) result(next)
        class(small_ring_counter), intent(in) :: this

        next = this%current
    end function get_next

    integer function get_next_and_increase(this, increase_by) result(next)
        class(small_ring_counter), intent(inout) :: this
        integer, intent(in), optional :: increase_by

        next = this%get_next()
        call this%increase(increase_by)
    end function get_next_and_increase

    integer function get_next_and_decrease(this, decrease_by) result(next)
        class(small_ring_counter), intent(inout) :: this
        integer, intent(in), optional :: decrease_by

        next = this%get_next()
        call this%decrease(decrease_by)
    end function get_next_and_decrease

    subroutine increase(this, increase_by)
        class(small_ring_counter), intent(inout) :: this
        integer, intent(in), optional :: increase_by

        integer :: increment

        increment = 1
        if ( present(increase_by) ) increment = increase_by

        if ( increment < 0 ) call this%decrease(-1*increment)
        this%current = this%get_counter_modulo(this%current+increment)
    end subroutine increase

    subroutine decrease(this, decrease_by)
        class(small_ring_counter), intent(inout) :: this
        integer, intent(in), optional :: decrease_by

        integer :: decrement

        decrement = 1
        if ( present(decrease_by) ) decrement = decrease_by

        if ( decrement < 0 ) call this%increase(-1*decrement)

        this%current = this%get_counter_modulo( &
                this%counter_size - decrement + this%current)
    end subroutine decrease

    subroutine reset(this)
        class(small_ring_counter), intent(inout) :: this

        this%current = 1
    end subroutine reset

    integer function get_counter_modulo(this, counter) result(c)
        class(small_ring_counter), intent(in) :: this
        integer, intent(in) :: counter

        c = mod(counter-1, this%counter_size) + 1
    end function get_counter_modulo

    subroutine clear(this)
        class(small_ring_counter), intent(inout) :: this

        call this%reset()
        this%counter_size = 0
    end subroutine clear
end module small_ring_counter_module
