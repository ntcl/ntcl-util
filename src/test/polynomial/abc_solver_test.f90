module abc_solver_test_module
    use, intrinsic :: iso_fortran_env, only : real64

    use :: util_api, only : &
            assert, &
            abc_solver

    implicit none
    private

    public :: abc_solver_test

    type :: abc_solver_test
    contains
        procedure :: run => run
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type abc_solver_test

    interface abc_solver_test
        module procedure constructor
    end interface abc_solver_test
contains
    function constructor() result(this)
        type(abc_solver_test) :: this

        call this%clear()
    end function constructor

    subroutine run(this, assertion)
        class(abc_solver_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        type(abc_solver) :: solver
        real(real64) :: solution

        call assertion%equal("abc_solver::Test complete", .true.)

        solver = abc_solver(1.0d0, 1.0d0, 0.0d0)
        call assertion%equal("abc_solver::Largest solution of n^2+n=0", &
            0 == solver%get_largest_solution())
        call assertion%equal("abc_solver::Smallest solution of n^2+n=0", &
            -1 == solver%get_smallest_solution())

        solver = abc_solver(1.0d0, 1.0d0, -6.0d0)
        call assertion%equal("abc_solver::Largest solution of n^2+n=6", &
            2 == solver%get_largest_solution())
        call assertion%equal("abc_solver::Smallest solution of n^2+n=6", &
            -3 == solver%get_smallest_solution())

        solver = abc_solver(1.0d0, 1.0d0, 6.0d0)
        solution = solver%get_largest_solution()
        call assertion%equal("abc_solver::Largest solution of n^2+n=-6::error", &
            solver%error)
        solution = solver%get_smallest_solution()
        call assertion%equal("abc_solver::Smallest solution of n^2+n=-6::error", &
            solver%error)

    end subroutine run

    subroutine cleanup(this)
        class(abc_solver_test), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(abc_solver_test), intent(inout) :: this
    end subroutine clear
end module abc_solver_test_module
