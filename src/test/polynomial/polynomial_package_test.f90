! Auto-generated -- DO NOT MODIFY
module polynomial_package_test_module
    use :: util_api, only : &
            selector, &
            assert

    use :: abc_solver_test_module, only : abc_solver_test

    implicit none
    private

    public :: polynomial_package_test

    type :: polynomial_package_test
        type(selector) :: test_selector
    contains
        procedure :: run => run
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type polynomial_package_test

    interface polynomial_package_test
        module procedure constructor
    end interface polynomial_package_test

contains
    function constructor(aselector) result(this)
        type(selector), intent(in) :: aselector
        type(polynomial_package_test) :: this

        call this%clear()

        this%test_selector = aselector
    end function constructor

    subroutine run(this, assertion)
        class(polynomial_package_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        type(abc_solver_test) :: aabc_solver_test

        call assertion%equal("polynomial::Package test complete", .true.)

        if ( &
                this%test_selector%is_enabled("abc_solver") ) then
            aabc_solver_test = abc_solver_test()
            call aabc_solver_test%run(assertion)
            call aabc_solver_test%cleanup()
        end if

    end subroutine run

    subroutine cleanup(this)
        class(polynomial_package_test), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(polynomial_package_test), intent(inout) :: this
    end subroutine clear
end module polynomial_package_test_module
