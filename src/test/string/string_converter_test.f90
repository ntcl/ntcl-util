module string_converter_test_module
    use, intrinsic :: iso_fortran_env, only : &
            real32, &
            real64

    use :: util_api, only : &
            assert, &
            string, &
            string_converter

    implicit none
    private

    public :: string_converter_test

    type :: string_converter_test
    contains
        procedure :: run => run
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type string_converter_test

    interface string_converter_test
        module procedure constructor
    end interface string_converter_test
contains
    function constructor() result(this)
        type(string_converter_test) :: this

        call this%clear()
    end function constructor

    subroutine run(this, assertion)
        class(string_converter_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        type(string_converter) :: converter
        type(string) :: dummy
        real(real32) :: r32
        real(real64) :: r64

        call assertion%equal("string_converter::Test complete", .true.)

        dummy = converter%to_string(11)
        call assertion%equal("string_converter::to_string:from int", &
                dummy == "11")
        dummy = converter%to_string(11.20)
        call assertion%equal("string_converter::to_string:from real32", &
                dummy == "11.2000")
        dummy = converter%to_string(real(11.2_real64, kind=real64))
        call assertion%equal("string_converter::to_string:from real32", &
                dummy == "11.20000000")
    end subroutine run

    subroutine cleanup(this)
        class(string_converter_test), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(string_converter_test), intent(inout) :: this
    end subroutine clear
end module string_converter_test_module
