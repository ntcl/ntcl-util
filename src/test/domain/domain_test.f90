module domain_test_module
    use :: util_api, only : &
            assert, &
            domain, &
            slice, &
            tile

    implicit none
    private

    public :: domain_test

    type :: domain_test
    contains
        procedure :: run => run
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type domain_test

    interface domain_test
        module procedure constructor
    end interface domain_test
contains
    function constructor() result(this)
        type(domain_test) :: this

        call this%clear()
    end function constructor

    subroutine run(this, assertion)
        class(domain_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        type(domain) :: src, dst
        type(tile) :: atile
        type(slice) :: aslice
        logical :: res

        call assertion%equal("domain::Test complete", .true.)

        src = domain([1,1], [5,5])
        dst = domain([1,1], [5,5])

        call assertion%equal("domain::Identical patches have overlap", &
                dst%has_overlap(src) )

        src = domain([1,1], [7,7])
        res = dst%has_overlap(domain([1,1], [7,7])) .and. &
                dst%has_overlap(domain([1,1], [3,3])) .and. &
                dst%has_overlap(domain([1,1], [3,7])) .and. &
                dst%has_overlap(domain([1,1], [7,3]))

        call assertion%equal("domain::Patches with no offsets have overlap", &
                res)

        res = dst%has_overlap(domain([1,1], [0,0])) .or. &
                dst%has_overlap(domain([1,1], [0,3])) .or. &
                dst%has_overlap(domain([1,1], [3,0])) .or. &
                dst%has_overlap(domain([1,1], [0,7])) .or. &
                dst%has_overlap(domain([1,1], [7,0]))

        call assertion%equal("domain::Patches with no size have no overlap", &
                .not. res)

        dst = domain([3,3], [7,7])
        res = dst%has_overlap(domain([1,1], [9,9])) .and. &
                dst%has_overlap(domain([4,4], [6,6])).and. &
                dst%has_overlap(domain([1,4], [9,6])) .and. &
                dst%has_overlap(domain([4,1], [6,9]))

        call assertion%equal("domain::Fully contained patches have overlap", &
                res)

        res = dst%has_overlap(domain([1,1], [2,2])) .or. &
                dst%has_overlap(domain([8,8], [9,9])).or. &
                dst%has_overlap(domain([1,4], [2,6])) .or. &
                dst%has_overlap(domain([4,1], [6,2])) .or. &
                dst%has_overlap(domain([1,8], [2,9])) .or. &
                dst%has_overlap(domain([8,1], [9,2]))

        call assertion%equal("domain::Patches without overlap", &
                .not. res)

        res = dst%has_overlap(domain([2,2], [6,6])) .and. &
                dst%has_overlap(domain([6,6], [9,9])) .and. &
                dst%has_overlap(domain([2,6], [6,9])) .and. &
                dst%has_overlap(domain([6,2], [9,6])) .and. &
                dst%has_overlap(domain([4,2], [6,6])) .and. &
                dst%has_overlap(domain([2,4], [6,6])) .and. &
                dst%has_overlap(domain([4,4], [6,9])) .and. &
                dst%has_overlap(domain([4,4], [9,6]))

        call assertion%equal("domain::Patches with partial overlap", &
                res)

        src = domain([1,1], [5,5])
        dst = domain([1,1], [5,5])

        res = dst%get_overlap(src) == dst .and. &
                dst%get_overlap(src) == src%get_overlap(dst)

        call assertion%equal("domain::Overlapping patch for equal patches", &
                res)

        dst = domain([3,3], [7,7])

        res = dst%get_overlap(domain([2,6], [6,9])) == domain([3,6], [6,7])

        call assertion%equal("domain::Overlapping patch for partially overlapping patches", &
                res)

        atile = tile(3,7,3,7)
        res = atile%get_overlap(tile(2,6,6,9)) == tile(3,6,6,7)

        call assertion%equal("domain::Overlapping tile for partially overlapping tile", &
                res)

        aslice = slice(3,7)
        res = aslice%get_overlap(slice(2,6)) == slice(3,6)

        call assertion%equal("domain::Overlapping slice for partially overlapping slice", &
                res)
    end subroutine run

    subroutine cleanup(this)
        class(domain_test), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(domain_test), intent(inout) :: this
    end subroutine clear
end module domain_test_module
