! Auto-generated -- DO NOT MODIFY
module counters_package_test_module
    use :: util_api, only : &
            selector, &
            assert

    use :: small_ring_counter_test_module, only : small_ring_counter_test

    implicit none
    private

    public :: counters_package_test

    type :: counters_package_test
        type(selector) :: test_selector
    contains
        procedure :: run => run
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type counters_package_test

    interface counters_package_test
        module procedure constructor
    end interface counters_package_test

contains
    function constructor(aselector) result(this)
        type(selector), intent(in) :: aselector
        type(counters_package_test) :: this

        call this%clear()

        this%test_selector = aselector
    end function constructor

    subroutine run(this, assertion)
        class(counters_package_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        type(small_ring_counter_test) :: asmall_ring_counter_test

        call assertion%equal("counters::Package test complete", .true.)

        if ( &
                this%test_selector%is_enabled("small_ring_counter") ) then
            asmall_ring_counter_test = small_ring_counter_test()
            call asmall_ring_counter_test%run(assertion)
            call asmall_ring_counter_test%cleanup()
        end if

    end subroutine run

    subroutine cleanup(this)
        class(counters_package_test), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(counters_package_test), intent(inout) :: this
    end subroutine clear
end module counters_package_test_module
