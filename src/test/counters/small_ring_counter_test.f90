module small_ring_counter_test_module
    use :: util_api, only : &
            assert, &
            small_ring_counter

    implicit none
    private

    public :: small_ring_counter_test

    type :: small_ring_counter_test
    contains
        procedure :: run => run
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type small_ring_counter_test

    interface small_ring_counter_test
        module procedure constructor
    end interface small_ring_counter_test
contains
    function constructor() result(this)
        type(small_ring_counter_test) :: this

        call this%clear()
    end function constructor

    subroutine run(this, assertion)
        class(small_ring_counter_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        type(small_ring_counter) :: asmall_ring_counter
        integer :: counter

        call assertion%equal("small_ring_counter::Test complete", .true.)

        asmall_ring_counter = small_ring_counter(10)
        call assertion%equal("small_ring_counter::Constructor:counter_size", &
                asmall_ring_counter%counter_size == 10)
        call assertion%equal("small_ring_counter::Constructor:current", &
                asmall_ring_counter%current == 1)

        asmall_ring_counter = small_ring_counter()
        call assertion%equal("small_ring_counter::Empty initialization:counter_size", &
                asmall_ring_counter%counter_size == 0)
        call assertion%equal("small_ring_counter::Empty initialization:current", &
                asmall_ring_counter%current == 1)

        asmall_ring_counter = small_ring_counter(10)
        call assertion%equal("small_ring_counter::After construction:get_next()", &
                asmall_ring_counter%get_next() == 1)
        call assertion%equal("small_ring_counter::After construction:get_next_and_increase()", &
                asmall_ring_counter%get_next_and_increase() == 1)
        call assertion%equal("small_ring_counter::After increase of 1:get_next()", &
                asmall_ring_counter%get_next() == 2)
        call assertion%equal("small_ring_counter::After increase of 1:get_next_and_increase(7)", &
                asmall_ring_counter%get_next_and_increase(7) == 2)
        call assertion%equal("small_ring_counter::After increase of 8:get_next()", &
                asmall_ring_counter%get_next() == 9)
        call asmall_ring_counter%increase(1)
        call assertion%equal("small_ring_counter::After increase of 9:get_next()", &
                asmall_ring_counter%get_next() == 10)
        call asmall_ring_counter%increase()
        call assertion%equal("small_ring_counter::After increase of 10:get_next()", &
                asmall_ring_counter%get_next() == 1)
        call asmall_ring_counter%increase(21)
        call assertion%equal("small_ring_counter::After increase of 31:get_next()", &
                asmall_ring_counter%get_next() == 2)

        call asmall_ring_counter%decrease()
        call assertion%equal("small_ring_counter::After increase of 31, decrease of 1:get_next()", &
                asmall_ring_counter%get_next() == 1)
        call asmall_ring_counter%decrease(1)
        call assertion%equal("small_ring_counter::After increase of 31, decrease of 2:get_next()", &
                asmall_ring_counter%get_next() == 10)
        call asmall_ring_counter%decrease(7)
        call assertion%equal("small_ring_counter::After increase of 31, decrease of 9:get_next()", &
                asmall_ring_counter%get_next() == 3)
        call asmall_ring_counter%decrease(22)
        call assertion%equal("small_ring_counter::After increase of 31, decrease of 31:get_next()", &
                asmall_ring_counter%get_next() == 1)
    end subroutine run

    subroutine cleanup(this)
        class(small_ring_counter_test), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(small_ring_counter_test), intent(inout) :: this
    end subroutine clear
end module small_ring_counter_test_module
