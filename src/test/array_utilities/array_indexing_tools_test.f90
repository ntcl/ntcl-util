module array_indexing_tools_test_module
    use, intrinsic :: iso_fortran_env, only : int64

    use :: util_api, only : &
            assert, &
            calculate_tensor_indices, &
            get_tensor_index_strides, &
            get_valid_index_map, &
            get_source_indices_from_tensor_indices, &
            get_destination_indices_from_tensor_indices, &
            array_view, &
            embedded_array_view, &
            calculate_local_array_indices_from_array_view, &
            calculate_global_array_indices_from_array_view, &
            calculate_vector_indices_from_local_array_indices, &
            calculate_vector_indices_from_global_array_indices, &
            calculate_vector_indices_from_array_view, &
            combine_local_tensor_indices_to_matrix_indices, &
            combine_global_tensor_indices_to_matrix_indices, &
            calculate_matrix_indices_from_tensor_view, &
            calculate_vector_indices_from_embedded_view


    implicit none
    private

    public :: array_indexing_tools_test

    type :: array_indexing_tools_test
    contains
        procedure :: run => run
        procedure :: cleanup => cleanup
        procedure :: clear => clear
        procedure, private :: test_array_view_routines => test_array_view_routines
        procedure, private :: test_rank2_array_view_routines => test_rank2_array_view_routines
        procedure, private :: test_rank3_array_view_routines => test_rank3_array_view_routines
        procedure, private :: test_rank2_tensor_in_matrix => test_rank2_tensor_in_matrix
        procedure, private :: test_rank3_tensor_in_matrix => test_rank3_tensor_in_matrix
    end type array_indexing_tools_test

    interface array_indexing_tools_test
        module procedure constructor
    end interface array_indexing_tools_test
contains
    function constructor() result(this)
        type(array_indexing_tools_test) :: this

        call this%clear()
    end function constructor

    subroutine run(this, assertion)
        class(array_indexing_tools_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        integer(int64), dimension(3,18) :: original
        integer(int64), dimension(:,:), allocatable :: indices
        integer(int64), dimension(:), allocatable :: array_indices
        integer(int64) :: offsets(3), sizes(3), zero(0)
        integer :: a, b, c, counter
        logical, dimension(18) :: valid


        call assertion%equal("array_indexing_tools::Test complete", .true.)

        sizes = [3,2,3]; offsets = [3,2,15]

        counter = 0
        do c = 1, sizes(3)
            do b = 1, sizes(2)
                do a = 1, sizes(1)
                    counter = counter + 1
                    original(1, counter) = offsets(1) + a
                    original(2, counter) = offsets(2) + b
                    original(3, counter) = offsets(3) + c
                end do
            end do
        end do
        call calculate_tensor_indices(indices, offsets, sizes)

        call assertion%is_equal("array_indexing_tools::calculate_tensor_indices", &
                original, indices)

        call assertion%is_equal("array_indexing_tools::get_tensor_index_strides", &
                int([1,3,6], kind=int64), get_tensor_index_strides(sizes))

        valid = .true.
        call assertion%equal("array_indexing_tools::get_valid_index_map for whole array", &
                all(valid .eqv. get_valid_index_map(original, offsets, sizes)))

        valid = .false.
        offsets = [10, 10, 20]
        call assertion%equal("array_indexing_tools::get_valid_index_map for non-overlapping array", &
                all(valid .eqv. get_valid_index_map(original, offsets, sizes)))

        valid = .true.
        valid(13:) = .false.
        offsets = [3, 2, 14]
        call assertion%equal("array_indexing_tools::get_valid_index_map for overlapping array 3rd index", &
                all(valid .eqv. get_valid_index_map(original, offsets, sizes)))

        valid = .true.
        valid(1::3) = .false.
        offsets = [4, 2, 15]
        call assertion%equal("array_indexing_tools::get_valid_index_map for overlapping array 1st index", &
                all(valid .eqv. get_valid_index_map(original, offsets, sizes)))

        array_indices = int([(a, a = 1, size(valid))], kind=int64)
        offsets = [3,2,15]
        call assertion%equal("array_indexing_tools::get_source_indices_from_tensor_indices for original array", &
                all(array_indices == get_source_indices_from_tensor_indices(original, offsets, sizes)))

        offsets = [0,0,0]
        call assertion%equal("array_indexing_tools::get_source_indices_from_tensor_indices for non-overlapping array", &
                size(get_source_indices_from_tensor_indices(original, offsets, sizes)) == 0)

        offsets = [3,2,14]
        array_indices = int([(a, a = 7, 18)], kind=int64)
        call assertion%equal("array_indexing_tools::get_source_indices_from_tensor_indices for overlapping array for 3rd", &
                all(get_source_indices_from_tensor_indices(original, offsets, sizes) == array_indices))

        offsets = [4,2,15]
        array_indices = [1, 2, 4, 5, 7, 8, 10, 11, 13, 14, 16, 17]
        call assertion%equal("array_indexing_tools::get_source_indices_from_tensor_indices for overlapping array for 1st", &
                all(get_source_indices_from_tensor_indices(original, offsets, sizes) == array_indices))

        array_indices = int([(a, a = 1, size(valid))], kind=int64)
        offsets = [3,2,15]
        call assertion%equal("array_indexing_tools::get_destination_indices_from_tensor_indices for original array", &
                all(array_indices == get_destination_indices_from_tensor_indices(original, offsets, sizes)))

        offsets = [0,0,0]
        call assertion%equal("array_indexing_tools::get_destination_indices_from_tensor_indices for non-overlapping array", &
                size(get_destination_indices_from_tensor_indices(original, offsets, sizes)) == 0)

        offsets = [3,2,14]
        array_indices = int([(a, a = 1, 12)], kind=int64)
        call assertion%equal("array_indexing_tools::"// &
                "get_destination_indices_from_tensor_indices for overlapping array for 3rd", &
                all(get_destination_indices_from_tensor_indices(original, offsets, sizes) == array_indices))

        offsets = [4,2,15]
        array_indices = [2, 3, 5, 6, 8, 9, 11, 12, 14, 15, 17, 18]
        call assertion%equal("array_indexing_tools::"// &
                "get_destination_indices_from_tensor_indices for overlapping array for 1st", &
                all(get_destination_indices_from_tensor_indices(original, offsets, sizes) == array_indices))

        call this%test_array_view_routines(assertion)
    end subroutine run

    subroutine test_array_view_routines(this, assertion)
        class(array_indexing_tools_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        call this%test_rank2_array_view_routines(assertion)
        call this%test_rank3_array_view_routines(assertion)

        call this%test_rank2_tensor_in_matrix(assertion)
        call this%test_rank3_tensor_in_matrix(assertion)
    end subroutine test_array_view_routines

    subroutine test_rank2_array_view_routines(this, assertion)
        class(array_indexing_tools_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        type(array_view) :: view
        integer(int64), dimension(:), allocatable :: vector_indices, &
                global_sizes, offsets, local_sizes, regression
        integer(int64), dimension(:,:), allocatable :: tensor_indices

        ! Full rank 2 view
        global_sizes = [2,3]
        offsets = [0,0]
        local_sizes = global_sizes
        view = array_view(global_sizes, offsets, local_sizes)

        call calculate_local_array_indices_from_array_view(tensor_indices, view)
        regression = [1, 1, 2, 1, 1, 2, 2, 2, 1, 3, 2, 3, 3, 3]
        call assertion%equal("array_indexing_tools::rank2::calculate_local_array_indices_from_array_view:"// &
                "Full view:Correct shape", &
                all(shape(tensor_indices) == [2,6]) )

        call assertion%equal("array_indexing_tools::rank2::calculate_local_array_indices_from_array_view:"// &
                "Full view:Correct elements", &
                all(tensor_indices == reshape(regression, shape=[2,6])))

        call calculate_vector_indices_from_local_array_indices(vector_indices, tensor_indices, view)
        regression = [1, 2, 3, 4, 5, 6]
        call assertion%equal("array_indexing_tools::rank2::calculate_vector_indices_from_local_array_indices:"// &
                "Full view:Correct size", &
                size(vector_indices) == size(regression) )

        call assertion%equal("array_indexing_tools::rank2::calculate_vector_indices_from_local_array_indices:"// &
                "Full view:Correct elements", &
                all(vector_indices==regression) )

        call calculate_global_array_indices_from_array_view(tensor_indices, view)
        regression = [1, 1, 2, 1, 1, 2, 2, 2, 1, 3, 2, 3, 3, 3]
        call assertion%equal("array_indexing_tools::rank2::calculate_global_array_indices_from_array_view:"// &
                "Full view:Correct shape", &
                all(shape(tensor_indices) == [2,6]) )
        call assertion%equal("array_indexing_tools::rank2::calculate_global_array_indices_from_array_view:"// &
                "Full view:Correct elements", &
                all(tensor_indices == reshape(regression, shape=[2,6])))

        call calculate_vector_indices_from_global_array_indices(vector_indices, tensor_indices, view)
        regression = [1, 2, 3, 4, 5, 6]
        call assertion%equal("array_indexing_tools::rank2::calculate_vector_indices_from_global_array_indices:"// &
                "Full view:Correct size", &
                size(vector_indices) == size(regression) )

        call assertion%equal("array_indexing_tools::rank2::calculate_vector_indices_from_global_array_indices:"// &
                "Full view:Correct elements", &
                all(vector_indices==regression) )

        call calculate_vector_indices_from_array_view(vector_indices, view)
        regression = [1, 2, 3, 4, 5, 6]
        call assertion%equal("array_indexing_tools::rank2::calculate_vector_indices_from_array_view:"// &
                "Full view:Correct size", &
                size(vector_indices) == size(regression) )

        call assertion%equal("array_indexing_tools::rank2::calculate_vector_indices_from_array_view:"// &
                "Full view:Correct elements", &
                all(vector_indices==regression) )

        ! Sliced rank 2 view
        global_sizes = [5,4]
        offsets = [1,1]
        local_sizes = [3,2]
        view = array_view(global_sizes, offsets, local_sizes)

        call calculate_local_array_indices_from_array_view(tensor_indices, view)
        regression = [1, 1, 2, 1, 3, 1, 1, 2, 2, 2, 3, 2]
        call assertion%equal("array_indexing_tools::rank2::calculate_local_array_indices_from_array_view:"// &
                "Sliced view:Correct shape", &
                all(shape(tensor_indices) == [2,6]) )
        call assertion%equal("array_indexing_tools::rank2::calculate_local_array_indices_from_array_view:"// &
                "Sliced view:Correct elements", &
                all(tensor_indices == reshape(regression, shape=[2,6])))

        call calculate_vector_indices_from_local_array_indices(vector_indices, tensor_indices, view)
        regression = [7,8,9,12,13,14]
        call assertion%equal("array_indexing_tools::rank2::calculate_vector_indices_from_local_array_indices:"// &
                "Full view:Correct size", &
                size(vector_indices) == size(regression) )

        call assertion%equal("array_indexing_tools::rank2::calculate_vector_indices_from_local_array_indices:"// &
                "Full view:Correct elements", &
                all(vector_indices==regression) )

        call calculate_global_array_indices_from_array_view(tensor_indices, view)
        regression = [2, 2, 3, 2, 4, 2, 2, 3, 3, 3, 4, 3]
        call assertion%equal("array_indexing_tools::rank2::calculate_global_array_indices_from_array_view:"// &
                "Sliced view:Correct shape", &
                all(shape(tensor_indices) == [2,6]) )
        call assertion%equal("array_indexing_tools::rank2::calculate_global_array_indices_from_array_view:"// &
                "Sliced view:Correct elements", &
                all(tensor_indices == reshape(regression, shape=[2,6])))

        call calculate_vector_indices_from_global_array_indices(vector_indices, tensor_indices, view)
        regression = [7,8,9,12,13,14]
        call assertion%equal("array_indexing_tools::rank2::calculate_vector_indices_from_global_array_indices:"// &
                "Full view:Correct size", &
                size(vector_indices) == size(regression) )

        call assertion%equal("array_indexing_tools::rank2::calculate_vector_indices_from_global_array_indices:"// &
                "Full view:Correct elements", &
                all(vector_indices==regression) )

        call calculate_vector_indices_from_array_view(vector_indices, view)
        regression = [7,8,9,12,13,14]
        call assertion%equal("array_indexing_tools::rank2::calculate_vector_indices_from_array_view:"// &
                "Sliced view:Correct size", &
                size(vector_indices) == size(regression) )

        call assertion%equal("array_indexing_tools::rank2::calculate_vector_indices_from_array_view:"// &
                "Sliced view:Correct elements", &
                all(vector_indices==regression) )

    end subroutine test_rank2_array_view_routines

    function get_global_rank3_regression_for_tensor_indices(view) result(regression)
        type(array_view), intent(in) :: view
        integer(int64), dimension(:), allocatable :: regression

        integer :: counter, i, j, k

        allocate(regression(product(view%local_sizes)*3))
        counter = 1
        do k = 1, view%local_sizes(3)
            do j = 1, view%local_sizes(2)
                do i = 1, view%local_sizes(1)
                    regression(counter:counter+2) = [view%offsets(1)+i,view%offsets(2)+j,view%offsets(3)+k]
                    counter = counter + 3
                end do
            end do
        end do
    end function get_global_rank3_regression_for_tensor_indices

    function get_local_rank3_regression_for_tensor_indices(view) result(regression)
        type(array_view), intent(in) :: view
        integer(int64), dimension(:), allocatable :: regression

        integer :: counter, i, j, k

        allocate(regression(product(view%local_sizes)*3))
        counter = 1
        do k = 1, view%local_sizes(3)
            do j = 1, view%local_sizes(2)
                do i = 1, view%local_sizes(1)
                    regression(counter:counter+2) = [i,j,k]
                    counter = counter + 3
                end do
            end do
        end do
    end function get_local_rank3_regression_for_tensor_indices

    function get_rank3_vector_indices(view) result(regression)
        type(array_view), intent(in) :: view
        integer(int64), dimension(:), allocatable :: regression

        integer :: counter, i, j, k

        allocate(regression(product(view%local_sizes)))
        counter = 0
        do k = view%offsets(3)+1, view%offsets(3)+view%local_sizes(3)
            do j = view%offsets(2)+1, view%offsets(2)+view%local_sizes(2)
                do i = view%offsets(1)+1, view%offsets(1)+view%local_sizes(1)
                    counter = counter + 1
                    regression(counter) = i + (j-1)*view%global_sizes(1) + (k-1)*product(view%global_sizes(1:2))
                end do
            end do
        end do
    end function get_rank3_vector_indices

    subroutine test_rank3_array_view_routines(this, assertion)
        class(array_indexing_tools_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        type(array_view) :: view
        integer(int64), dimension(:), allocatable :: vector_indices, &
                global_sizes, offsets, local_sizes, regression
        integer(int64), dimension(:,:), allocatable :: tensor_indices

        ! Full rank 3 view
        global_sizes = [2,3,3]
        offsets = [0,0,0]
        local_sizes = global_sizes
        view = array_view(global_sizes, offsets, local_sizes)

        call calculate_local_array_indices_from_array_view(tensor_indices, view)
        regression = get_local_rank3_regression_for_tensor_indices(view)
        call assertion%equal("array_indexing_tools::rank3::calculate_local_array_indices_from_array_view:"// &
                "Full view:Correct shape", &
                all(shape(tensor_indices) == [3,18]) )

        call assertion%equal("array_indexing_tools::rank3::calculate_local_array_indices_from_array_view:"// &
                "Full view:Correct elements", &
                all(tensor_indices == reshape(regression, shape=[3,18])))

        call calculate_vector_indices_from_local_array_indices(vector_indices, tensor_indices, view)
        regression = get_rank3_vector_indices(view)

        call assertion%equal("array_indexing_tools::rank3::calculate_vector_indices_from_local_array_indices:"// &
                "Full view:Correct size", &
                size(vector_indices) == size(regression) )

        call assertion%equal("array_indexing_tools::rank3::calculate_vector_indices_from_local_array_indices:"// &
                "Full view:Correct elements", &
                all(vector_indices==regression) )

        call calculate_global_array_indices_from_array_view(tensor_indices, view)
        regression = get_global_rank3_regression_for_tensor_indices(view)
        call assertion%equal("array_indexing_tools::rank3::calculate_global_array_indices_from_array_view:"// &
                "Full view:Correct shape", &
                all(shape(tensor_indices) == [3,18]) )
        call assertion%equal("array_indexing_tools::rank3::calculate_global_array_indices_from_array_view:"// &
                "Full view:Correct elements", &
                all(tensor_indices == reshape(regression, shape=[3,18])))

        call calculate_vector_indices_from_global_array_indices(vector_indices, tensor_indices, view)
        regression = get_rank3_vector_indices(view)
        call assertion%equal("array_indexing_tools::rank3::calculate_vector_indices_from_global_array_indices:"// &
                "Full view:Correct size", &
                size(vector_indices) == size(regression) )

        call assertion%equal("array_indexing_tools::rank3::calculate_vector_indices_from_global_array_indices:"// &
                "Full view:Correct elements", &
                all(vector_indices==regression) )

        call calculate_vector_indices_from_array_view(vector_indices, view)
        regression = get_rank3_vector_indices(view)
        call assertion%equal("array_indexing_tools::rank3::calculate_vector_indices_from_array_view:"// &
                "Full view:Correct size", &
                size(vector_indices) == size(regression) )

        call assertion%equal("array_indexing_tools::rank3::calculate_vector_indices_from_array_view:"// &
                "Full view:Correct elements", &
                all(vector_indices==regression) )

        ! Sliced rank 3 view
        global_sizes = [5,4,5]
        offsets = [1,1,1]
        local_sizes = [3,2,3]
        view = array_view(global_sizes, offsets, local_sizes)

        call calculate_local_array_indices_from_array_view(tensor_indices, view)
        regression = get_local_rank3_regression_for_tensor_indices(view)
        call assertion%equal("array_indexing_tools::rank3::calculate_local_array_indices_from_array_view:"// &
                "Sliced view:Correct shape", &
                all(shape(tensor_indices) == [3,18]) )

        call assertion%equal("array_indexing_tools::rank3::calculate_local_array_indices_from_array_view:"// &
                "Sliced view:Correct elements", &
                all(tensor_indices == reshape(regression, shape=[3,18])))

        call calculate_vector_indices_from_local_array_indices(vector_indices, tensor_indices, view)
        regression = get_rank3_vector_indices(view)

        call assertion%equal("array_indexing_tools::rank3::calculate_vector_indices_from_local_array_indices:"// &
                "Sliced view:Correct size", &
                size(vector_indices) == size(regression) )

        call assertion%equal("array_indexing_tools::rank3::calculate_vector_indices_from_local_array_indices:"// &
                "Sliced view:Correct elements", &
                all(vector_indices==regression) )

        call calculate_global_array_indices_from_array_view(tensor_indices, view)
        regression = get_global_rank3_regression_for_tensor_indices(view)
        call assertion%equal("array_indexing_tools::rank3::calculate_global_array_indices_from_array_view:"// &
                "Sliced view:Correct shape", &
                all(shape(tensor_indices) == [3,18]) )
        call assertion%equal("array_indexing_tools::rank3::calculate_global_array_indices_from_array_view:"// &
                "Sliced view:Correct elements", &
                all(tensor_indices == reshape(regression, shape=[3,18])))

        call calculate_vector_indices_from_global_array_indices(vector_indices, tensor_indices, view)
        regression = get_rank3_vector_indices(view)
        call assertion%equal("array_indexing_tools::rank3::calculate_vector_indices_from_global_array_indices:"// &
                "Sliced view:Correct size", &
                size(vector_indices) == size(regression) )

        call assertion%equal("array_indexing_tools::rank3::calculate_vector_indices_from_global_array_indices:"// &
                "Sliced view:Correct elements", &
                all(vector_indices==regression) )

        call calculate_vector_indices_from_array_view(vector_indices, view)
        regression = get_rank3_vector_indices(view)
        call assertion%equal("array_indexing_tools::rank3::calculate_vector_indices_from_array_view:"// &
                "Sliced view:Correct size", &
                size(vector_indices) == size(regression) )

        call assertion%equal("array_indexing_tools::rank3::calculate_vector_indices_from_array_view:"// &
                "Sliced view:Correct elements", &
                all(vector_indices==regression) )
    end subroutine test_rank3_array_view_routines

    function get_matrix_indices_from_rank2_tensor(tensor) result(regression)
        type(array_view), intent(in) :: tensor
        integer(int64), dimension(:), allocatable :: regression

        integer :: counter, i, j, i_idx, j_idx

        allocate(regression(product(tensor%local_sizes)*2))
        counter = 1
        do j = 1, tensor%local_sizes(2)
            j_idx = tensor%offsets(2) + j
            do i = 1, tensor%local_sizes(1)
                i_idx = tensor%offsets(2) + i
                regression(counter:counter+1) = [i_idx, j_idx]
                counter = counter + 2
            end do
        end do
    end function get_matrix_indices_from_rank2_tensor

    function get_matrix_indices_from_rank3_tensor(tensor) result(regression)
        type(array_view), intent(in) :: tensor
        integer(int64), dimension(:), allocatable :: regression

        integer :: counter, i, j, k, i_idx, j_idx, k_idx, ij_idx

        allocate(regression(product(tensor%local_sizes)*2))
        counter = 1
        do k = 1, tensor%local_sizes(3)
            k_idx = tensor%offsets(3) + k
            do j = 1, tensor%local_sizes(2)
                j_idx = tensor%offsets(2) + j
                do i = 1, tensor%local_sizes(1)
                    i_idx = tensor%offsets(1) + i
                    ij_idx = i_idx + (j_idx-1)*tensor%global_sizes(1)
                    regression(counter:counter+1) = [ij_idx, k_idx]
                    counter = counter + 2
                end do
            end do
        end do
    end function get_matrix_indices_from_rank3_tensor

    function get_vector_indices_from_rank2_embedded(tensor, matrix) result(regression)
        type(array_view), intent(in) :: tensor, matrix
        integer(int64), dimension(:), allocatable :: regression

        integer :: i, j, i_idx, j_idx, idx, nsize
        integer(int64), dimension(:,:), allocatable :: matrix_indices

        nsize = product(tensor%local_sizes)
        matrix_indices = reshape(get_matrix_indices_from_rank2_tensor(tensor), shape=[2, nsize])
        allocate(regression(nsize))

        do idx = 1, nsize
            i = matrix_indices(1, idx)
            i_idx = matrix%offsets(1) + i
            j = matrix_indices(2, idx)
            j_idx = matrix%offsets(2) + j
            regression(idx) = i_idx + (j_idx-1)*matrix%global_sizes(1)
        end do
    end function get_vector_indices_from_rank2_embedded

    function get_vector_indices_from_rank3_embedded(tensor, matrix) result(regression)
        type(array_view), intent(in) :: tensor, matrix
        integer(int64), dimension(:), allocatable :: regression

        integer :: i, j, i_idx, j_idx, counter, idx, nsize
        integer(int64), dimension(:,:), allocatable :: matrix_indices

        nsize = product(tensor%local_sizes)
        matrix_indices = reshape(get_matrix_indices_from_rank3_tensor(tensor), shape=[2, nsize])
        allocate(regression(nsize))

        do idx = 1, nsize
            i = matrix_indices(1, idx)
            i_idx = matrix%offsets(1) + i
            j = matrix_indices(2, idx)
            j_idx = matrix%offsets(2) + j
            regression(idx) = i_idx + (j_idx-1)*matrix%global_sizes(1)
        end do
    end function get_vector_indices_from_rank3_embedded

    subroutine test_rank2_tensor_in_matrix(this, assertion)
        class(array_indexing_tools_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        type(array_view) :: matrix, tensor
        integer(int64), dimension(:), allocatable :: vector_indices, &
                global_sizes, offsets, local_sizes, regression
        integer(int64), dimension(:,:), allocatable :: tensor_indices, matrix_indices
        type(embedded_array_view) :: view

        global_sizes = [4,3]
        offsets = [0,0]
        local_sizes = [4,3]
        tensor = array_view(global_sizes, offsets, local_sizes)

        call calculate_local_array_indices_from_array_view(tensor_indices, tensor)

        call combine_local_tensor_indices_to_matrix_indices(matrix_indices, tensor_indices, &
                tensor, 1)
        regression = get_matrix_indices_from_rank2_tensor(tensor)

        call assertion%equal("array_indexing_tools::rank2::combine_local_tensor_indices_to_matrix_indices:"// &
                "Full view:Correct shape", &
                all(shape(matrix_indices) == [2, 12]))

        call assertion%equal("array_indexing_tools::rank2::combine_local_tensor_indices_to_matrix_indices:"// &
                "Full view:Correct elements", &
                all(matrix_indices == reshape(regression, shape=[2, 12])))

        call calculate_global_array_indices_from_array_view(tensor_indices, tensor)

        call combine_global_tensor_indices_to_matrix_indices(matrix_indices, tensor_indices, &
                tensor, 1)

        call assertion%equal("array_indexing_tools::rank2::combine_global_tensor_indices_to_matrix_indices:"// &
                "Full view:Correct shape", &
                all(shape(matrix_indices) == [2, 12]))

        call assertion%equal("array_indexing_tools::rank2::combine_global_tensor_indices_to_matrix_indices:"// &
                "Full view:Correct elements", &
                all(matrix_indices == reshape(regression, shape=[2, 12])))

        call calculate_matrix_indices_from_tensor_view(matrix_indices, tensor, 1)

        call assertion%equal("array_indexing_tools::rank2::calculate_matrix_indices_from_tensor_view:"// &
                "Full view:Correct shape", &
                all(shape(matrix_indices) == [2, 12]))

        call assertion%equal("array_indexing_tools::rank2::calculate_matrix_indices_from_tensor_view:"// &
                "Full view:Correct elements", &
                all(matrix_indices == reshape(regression, shape=[2, 12])))

        matrix = tensor
        view = embedded_array_view(tensor, matrix, 1)
        call calculate_vector_indices_from_embedded_view(vector_indices, view)
        regression = get_vector_indices_from_rank2_embedded(tensor, matrix)
        call assertion%equal("array_indexing_tools::rank2::calculate_vector_indices_from_embedded_view:"// &
                "Full view, no embedding:Correct size", &
                size(vector_indices) == size(regression))

        call assertion%equal("array_indexing_tools::rank2::calculate_vector_indices_from_embedded_view:"// &
                "Full view, no embedding:Correct elements", &
                all(vector_indices == regression))

        matrix = array_view(int([12,11], int64), int([3,4], int64), int([4,3], int64))
        view = embedded_array_view(tensor, matrix, 1)
        call calculate_vector_indices_from_embedded_view(vector_indices, view)
        regression = get_vector_indices_from_rank2_embedded(tensor, matrix)
        call assertion%equal("array_indexing_tools::rank2::calculate_vector_indices_from_embedded_view:"// &
                "Full view, embedded:Correct size", &
                size(vector_indices) == size(regression))

        call assertion%equal("array_indexing_tools::rank2::calculate_vector_indices_from_embedded_view:"// &
                "Full view, embedded:Correct elements", &
                all(vector_indices == regression))

        global_sizes = [6,5]
        offsets = [1,1]
        local_sizes = [4,3]
        tensor = array_view(global_sizes, offsets, local_sizes)

        call calculate_local_array_indices_from_array_view(tensor_indices, tensor)

        call combine_local_tensor_indices_to_matrix_indices(matrix_indices, tensor_indices, &
                tensor, 1)
        regression = get_matrix_indices_from_rank2_tensor(tensor)

        call assertion%equal("array_indexing_tools::rank2::combine_local_tensor_indices_to_matrix_indices:"// &
                "Sliced view:Correct shape", &
                all(shape(matrix_indices) == [2, 12]))

        call assertion%equal("array_indexing_tools::rank2::combine_local_tensor_indices_to_matrix_indices:"// &
                "Sliced view:Correct elements", &
                all(matrix_indices == reshape(regression, shape=[2, 12])))

        call calculate_global_array_indices_from_array_view(tensor_indices, tensor)

        call combine_global_tensor_indices_to_matrix_indices(matrix_indices, tensor_indices, &
                tensor, 1)

        call assertion%equal("array_indexing_tools::rank2::combine_global_tensor_indices_to_matrix_indices:"// &
                "Sliced view:Correct shape", &
                all(shape(matrix_indices) == [2, 12]))

        call assertion%equal("array_indexing_tools::rank2::combine_global_tensor_indices_to_matrix_indices:"// &
                "Sliced view:Correct elements", &
                all(matrix_indices == reshape(regression, shape=[2, 12])))

        call calculate_matrix_indices_from_tensor_view(matrix_indices, tensor, 1)

        call assertion%equal("array_indexing_tools::rank2::calculate_matrix_indices_from_tensor_view:"// &
                "Sliced view:Correct shape", &
                all(shape(matrix_indices) == [2, 12]))

        call assertion%equal("array_indexing_tools::rank2::calculate_matrix_indices_from_tensor_view:"// &
                "Sliced view:Correct elements", &
                all(matrix_indices == reshape(regression, shape=[2, 12])))

        matrix = array_view(int([6,5], int64), int([0,0], int64), int([6,6], int64))
        view = embedded_array_view(tensor, matrix, 1)
        call calculate_vector_indices_from_embedded_view(vector_indices, view)
        regression = get_vector_indices_from_rank2_embedded(tensor, matrix)
        call assertion%equal("array_indexing_tools::rank2::calculate_vector_indices_from_embedded_view:"// &
                "Sliced view, no embedding:Correct size", &
                size(vector_indices) == size(regression))

        call assertion%equal("array_indexing_tools::rank2::calculate_vector_indices_from_embedded_view:"// &
                "Sliced view, no embedding:Correct elements", &
                all(vector_indices == regression))

        matrix = array_view(int([12,11], int64), int([3,4], int64), int([4,3], int64))
        view = embedded_array_view(tensor, matrix, 1)
        call calculate_vector_indices_from_embedded_view(vector_indices, view)
        regression = get_vector_indices_from_rank2_embedded(tensor, matrix)
        call assertion%equal("array_indexing_tools::rank2::calculate_vector_indices_from_embedded_view:"// &
                "Sliced view, embedded:Correct size", &
                size(vector_indices) == size(regression))

        call assertion%equal("array_indexing_tools::rank2::calculate_vector_indices_from_embedded_view:"// &
                "Sliced view, embedded:Correct elements", &
                all(vector_indices == regression))


    end subroutine test_rank2_tensor_in_matrix

    subroutine test_rank3_tensor_in_matrix(this, assertion)
        class(array_indexing_tools_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        type(array_view) :: matrix, tensor
        integer(int64), dimension(:), allocatable :: vector_indices, &
                global_sizes, offsets, local_sizes, regression
        integer(int64), dimension(:,:), allocatable :: tensor_indices, matrix_indices
        type(embedded_array_view) :: view

        global_sizes = [4,3,3]
        offsets = [0,0,0]
        local_sizes = [4,3,3]
        tensor = array_view(global_sizes, offsets, local_sizes)

        call calculate_local_array_indices_from_array_view(tensor_indices, tensor)

        call combine_local_tensor_indices_to_matrix_indices(matrix_indices, tensor_indices, &
                tensor, 2)
        regression = get_matrix_indices_from_rank3_tensor(tensor)

        call assertion%equal("array_indexing_tools::rank3::combine_local_tensor_indices_to_matrix_indices:"// &
                "Full view:Correct shape", &
                all(shape(matrix_indices) == [2, 36]))

        call assertion%equal("array_indexing_tools::rank3::combine_local_tensor_indices_to_matrix_indices:"// &
                "Full view:Correct elements", &
                all(matrix_indices == reshape(regression, shape=[2, 36])))

        call calculate_global_array_indices_from_array_view(tensor_indices, tensor)

        call combine_global_tensor_indices_to_matrix_indices(matrix_indices, tensor_indices, &
                tensor, 2)

        call assertion%equal("array_indexing_tools::rank3::combine_global_tensor_indices_to_matrix_indices:"// &
                "Full view:Correct shape", &
                all(shape(matrix_indices) == [2, 36]))

        call assertion%equal("array_indexing_tools::rank3::combine_global_tensor_indices_to_matrix_indices:"// &
                "Full view:Correct elements", &
                all(matrix_indices == reshape(regression, shape=[2, 36])))

        call calculate_matrix_indices_from_tensor_view(matrix_indices, tensor, 2)

        call assertion%equal("array_indexing_tools::rank3::calculate_matrix_indices_from_tensor_view:"// &
                "Full view:Correct shape", &
                all(shape(matrix_indices) == [2, 36]))

        call assertion%equal("array_indexing_tools::rank3::calculate_matrix_indices_from_tensor_view:"// &
                "Full view:Correct elements", &
                all(matrix_indices == reshape(regression, shape=[2, 36])))

        matrix = array_view(int([12,3], int64), int([0,0], int64), int([12,3], int64))
        view = embedded_array_view(tensor, matrix, 2)
        call calculate_vector_indices_from_embedded_view(vector_indices, view)
        regression = get_vector_indices_from_rank3_embedded(tensor, matrix)
        call assertion%equal("array_indexing_tools::rank3::calculate_vector_indices_from_embedded_view:"// &
                "Full view, no embedding:Correct size", &
                size(vector_indices) == size(regression))

        call assertion%equal("array_indexing_tools::rank3::calculate_vector_indices_from_embedded_view:"// &
                "Full view, no embedding:Correct elements", &
                all(vector_indices == regression))

        matrix = array_view(int([40,33], int64), int([3,4], int64), int([12,3], int64))
        view = embedded_array_view(tensor, matrix, 2)
        call calculate_vector_indices_from_embedded_view(vector_indices, view)
        regression = get_vector_indices_from_rank3_embedded(tensor, matrix)
        call assertion%equal("array_indexing_tools::rank3::calculate_vector_indices_from_embedded_view:"// &
                "Full view, embedded:Correct size", &
                size(vector_indices) == size(regression))

        call assertion%equal("array_indexing_tools::rank3::calculate_vector_indices_from_embedded_view:"// &
                "Full view, embedded:Correct elements", &
                all(vector_indices == regression))

        global_sizes = [6,7,9]
        offsets = [1,2,3]
        local_sizes = [4,3,5]
        tensor = array_view(global_sizes, offsets, local_sizes)

        call calculate_local_array_indices_from_array_view(tensor_indices, tensor)

        call combine_local_tensor_indices_to_matrix_indices(matrix_indices, tensor_indices, &
                tensor, 2)
        regression = get_matrix_indices_from_rank3_tensor(tensor)

        call assertion%equal("array_indexing_tools::rank3::combine_local_tensor_indices_to_matrix_indices:"// &
                "Sliced view:Correct shape", &
                all(shape(matrix_indices) == [2, 60]))

        call assertion%equal("array_indexing_tools::rank3::combine_local_tensor_indices_to_matrix_indices:"// &
                "Sliced view:Correct elements", &
                all(matrix_indices == reshape(regression, shape=[2, 60])))

        call calculate_global_array_indices_from_array_view(tensor_indices, tensor)

        call combine_global_tensor_indices_to_matrix_indices(matrix_indices, tensor_indices, &
                tensor, 2)

        call assertion%equal("array_indexing_tools::rank3::combine_global_tensor_indices_to_matrix_indices:"// &
                "Sliced view:Correct shape", &
                all(shape(matrix_indices) == [2, 60]))

        call assertion%equal("array_indexing_tools::rank3::combine_global_tensor_indices_to_matrix_indices:"// &
                "Sliced view:Correct elements", &
                all(matrix_indices == reshape(regression, shape=[2, 60])))

        call calculate_matrix_indices_from_tensor_view(matrix_indices, tensor, 2)

        call assertion%equal("array_indexing_tools::rank3::calculate_matrix_indices_from_tensor_view:"// &
                "Sliced view:Correct shape", &
                all(shape(matrix_indices) == [2, 60]))

        call assertion%equal("array_indexing_tools::rank3::calculate_matrix_indices_from_tensor_view:"// &
                "Sliced view:Correct elements", &
                all(matrix_indices == reshape(regression, shape=[2, 60])))

        matrix = array_view(int([30,7], int64), int([0,0], int64), int([30,7], int64))
        view = embedded_array_view(tensor, matrix, 2)
        call calculate_vector_indices_from_embedded_view(vector_indices, view)
        regression = get_vector_indices_from_rank3_embedded(tensor, matrix)
        call assertion%equal("array_indexing_tools::rank3::calculate_vector_indices_from_embedded_view:"// &
                "Sliced view, no embedding:Correct size", &
                size(vector_indices) == size(regression))

        call assertion%equal("array_indexing_tools::rank3::calculate_vector_indices_from_embedded_view:"// &
                "Sliced view, no embedding:Correct elements", &
                all(vector_indices == regression))

        matrix = array_view(int([40,33], int64), int([3,4], int64), int([12,3], int64))
        view = embedded_array_view(tensor, matrix, 2)
        call calculate_vector_indices_from_embedded_view(vector_indices, view)
        regression = get_vector_indices_from_rank3_embedded(tensor, matrix)
        call assertion%equal("array_indexing_tools::rank3::calculate_vector_indices_from_embedded_view:"// &
                "Sliced view, embedded:Correct size", &
                size(vector_indices) == size(regression))

        call assertion%equal("array_indexing_tools::rank3::calculate_vector_indices_from_embedded_view:"// &
                "Sliced view, embedded:Correct elements", &
                all(vector_indices == regression))

    end subroutine test_rank3_tensor_in_matrix

    subroutine cleanup(this)
        class(array_indexing_tools_test), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(array_indexing_tools_test), intent(inout) :: this
    end subroutine clear
end module array_indexing_tools_test_module
