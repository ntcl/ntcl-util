module sparse_index_map_translation_test_module
    use, intrinsic :: iso_fortran_env, only : &
            int64

    use :: util_api, only : &
            assert, &
            domain, &
            sparse_index_map, &
            translate_sparse_index_map

    implicit none
    private

    public :: sparse_index_map_translation_test

    type :: sparse_index_map_translation_test
    contains
        procedure :: run => run
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type sparse_index_map_translation_test

    interface sparse_index_map_translation_test
        module procedure constructor
    end interface sparse_index_map_translation_test
contains
    function constructor() result(this)
        type(sparse_index_map_translation_test) :: this

        call this%clear()
    end function constructor

    subroutine run(this, assertion)
        class(sparse_index_map_translation_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        type(sparse_index_map) :: dst, src
        integer, dimension(:,:), allocatable :: maps
        integer(int64), dimension(:,:), allocatable :: map

        call assertion%equal("sparse_index_map_translation::Test complete", .true.)

        ! Test identical maps
        allocate(maps(4,1))
        maps(:,1) = [1,2,3,4]
        dst = sparse_index_map(maps, calculate_reverse_maps=.true.)
        src = sparse_index_map(maps, calculate_reverse_maps=.true.)

        call translate_sparse_index_map(map, dst, src)
        call assertion%is_equal("sparse_index_map_translation::Identical maps - dst", &
                int([1, 2, 3, 4], kind=int64), map(:,1))
        call assertion%is_equal("sparse_index_map_translation::Identical maps - src", &
                int([1, 2, 3, 4], kind=int64), map(:,2))
        deallocate(map)

        maps(:,1) = [4,3,2,1]
        src = sparse_index_map(maps, calculate_reverse_maps=.true.)

        call translate_sparse_index_map(map, dst, src)
        call assertion%is_equal("sparse_index_map_translation::Reverse maps - dst", &
                int([1, 2, 3, 4], kind=int64), map(:,1))
        call assertion%is_equal("sparse_index_map_translation::Reverse maps - src", &
                int([4, 3, 2, 1], kind=int64), map(:,2))
        deallocate(map)

        maps(:,1) = [1,2,3,4]
        dst = sparse_index_map(maps, sparse_domain=domain([1],[8]), calculate_reverse_maps=.true.)
        maps(:,1) = [4, 5, 6, 7]
        src = sparse_index_map(maps, sparse_domain=domain([1],[8]), calculate_reverse_maps=.true.)

        call translate_sparse_index_map(map, dst, src)
        call assertion%is_equal("sparse_index_map_translation::Random maps v1 - dst", &
                int([4], kind=int64), map(:,1))
        call assertion%is_equal("sparse_index_map_translation::Random maps v1 - src", &
                int([1], kind=int64), map(:,2))
        deallocate(map)

        maps(:,1) = [1,2,3,4]
        dst = sparse_index_map(maps, sparse_domain=domain([1],[8]), calculate_reverse_maps=.true.)
        maps(:,1) = [7, 4, 2, 6, 5, 8]
        src = sparse_index_map(maps, sparse_domain=domain([1],[8]), calculate_reverse_maps=.true.)

        call translate_sparse_index_map(map, dst, src)
        call assertion%is_equal("sparse_index_map_translation::Random maps v2 - dst", &
                int([2,4], kind=int64), map(:,1))
        call assertion%is_equal("sparse_index_map_translation::Random maps v2 - src", &
                int([3,2], kind=int64), map(:,2))
        deallocate(map)

        maps = reshape([1,2,4,6,3,5,3,4,6,8,4,6], shape=[6,2])
        dst = sparse_index_map(maps, sparse_domain=domain([1,1],[8,8]), calculate_reverse_maps=.true.)
        maps = reshape([1,3,2,2,4,5,7,3,4,6], shape=[5,2])
        src = sparse_index_map(maps, sparse_domain=domain([1,1],[8,8]), calculate_reverse_maps=.true.)

        call translate_sparse_index_map(map, dst, src)
        call assertion%is_equal("sparse_index_map_translation::Random maps v3 - dst", &
                int([2,3], kind=int64), map(:,1))
        call assertion%is_equal("sparse_index_map_translation::Random maps v3 - src", &
                int([4,5], kind=int64), map(:,2))
        deallocate(map)

        maps = reshape([3,4,4,6,3,5,3,4,6,8,5,6], shape=[6,2])
        dst = sparse_index_map(maps, sparse_domain=domain([3,3],[8,8]), calculate_reverse_maps=.true.)
        maps = reshape([3,3,4,4,4,5,7,3,4,6], shape=[5,2])
        src = sparse_index_map(maps, sparse_domain=domain([3,3],[8,8]), calculate_reverse_maps=.true.)

        call translate_sparse_index_map(map, dst, src)
        call assertion%is_equal("sparse_index_map_translation::Random maps with offset v1 - dst", &
                int([2,3,5], kind=int64), map(:,1))
        call assertion%is_equal("sparse_index_map_translation::Random maps with offset v1 - src", &
                int([4,5,1], kind=int64), map(:,2))
        deallocate(map)

    end subroutine run

    subroutine cleanup(this)
        class(sparse_index_map_translation_test), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(sparse_index_map_translation_test), intent(inout) :: this
    end subroutine clear
end module sparse_index_map_translation_test_module
