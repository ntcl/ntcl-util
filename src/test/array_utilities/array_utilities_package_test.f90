! Auto-generated -- DO NOT MODIFY
module array_utilities_package_test_module
    use :: util_api, only : &
            selector, &
            assert

    use :: array_indexing_tools_test_module, only : array_indexing_tools_test
    use :: sparse_index_map_translation_test_module, only : sparse_index_map_translation_test

    implicit none
    private

    public :: array_utilities_package_test

    type :: array_utilities_package_test
        type(selector) :: test_selector
    contains
        procedure :: run => run
        procedure :: cleanup => cleanup
        procedure :: clear => clear
    end type array_utilities_package_test

    interface array_utilities_package_test
        module procedure constructor
    end interface array_utilities_package_test

contains
    function constructor(aselector) result(this)
        type(selector), intent(in) :: aselector
        type(array_utilities_package_test) :: this

        call this%clear()

        this%test_selector = aselector
    end function constructor

    subroutine run(this, assertion)
        class(array_utilities_package_test), intent(in) :: this
        type(assert), intent(inout) :: assertion

        type(array_indexing_tools_test) :: aarray_indexing_tools_test
        type(sparse_index_map_translation_test) :: asparse_index_map_translation_test

        call assertion%equal("array_utilities::Package test complete", .true.)

        if ( &
                this%test_selector%is_enabled("array_indexing_tools") ) then
            aarray_indexing_tools_test = array_indexing_tools_test()
            call aarray_indexing_tools_test%run(assertion)
            call aarray_indexing_tools_test%cleanup()
        end if

        if ( &
                this%test_selector%is_enabled("sparse_index_map_translation") ) then
            asparse_index_map_translation_test = sparse_index_map_translation_test()
            call asparse_index_map_translation_test%run(assertion)
            call asparse_index_map_translation_test%cleanup()
        end if

    end subroutine run

    subroutine cleanup(this)
        class(array_utilities_package_test), intent(inout) :: this

        call this%clear()
    end subroutine cleanup

    subroutine clear(this)
        class(array_utilities_package_test), intent(inout) :: this
    end subroutine clear
end module array_utilities_package_test_module
